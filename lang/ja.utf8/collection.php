<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-10-19 15:53:51 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'コレクション';
$string['about'] = 'About';
$string['access'] = 'アクセス';
$string['accesscantbeused'] = 'アクセスオーバーライドは保存されませんでした。選択されたページアクセス (シークレットURL) は複数のページで使用することができません。';
$string['accessoverride'] = 'アクセスオーバーライド';
$string['accesssaved'] = 'コレクションアクセスが正常に保存されました。';
$string['accessignored'] = 'いくつかのシークレットURLタイプは無視されました。';
$string['add'] = '追加';
$string['addviews'] = 'ページを追加する';
$string['addviewstocollection'] = 'コレクションにページを追加する';
$string['back'] = '戻る';
$string['cantlistgroupcollections'] = 'あなたはグループコレクションの一覧表示を許可されていません。';
$string['cantlistinstitutioncollections'] = 'あなたはインスティテューションコレクションの一覧表示を許可されていません。';
$string['canteditgroupcollections'] = 'あなたはグループコレクションの編集を許可されていません。';
$string['canteditinstitutioncollections'] = 'あなたはインスティテューションコレクションの編集を許可されていません。';
$string['canteditcollection'] = 'あなたはこのコレクションの編集を許可されていません。';
$string['cantcreatecollection'] = 'あなたはこのコレクションの作成を許可されていません。';
$string['cantdeletecollection'] = 'あなたはこのコレクションの削除を許可されていません。';
$string['canteditdontown'] = 'あなたの所有ではないため、このコレクションを編集することはできません。';
$string['canteditsubmitted'] = '評価のため「 %s 」に送信されたため、あなたはこのコレクションを編集することはできません。リリースされるまで、あなたは待つ必要があります。';
$string['collection'] = 'コレクション';
$string['Collection'] = 'コレクション';
$string['collections'] = 'コレクション';
$string['Collections'] = 'コレクション';
$string['groupcollections'] = 'グループコレクション';
$string['institutioncollections'] = 'インスティテューションコレクション';
$string['sitecollections'] = 'サイトコレクション';
$string['collectionaccess'] = 'コレクションアクセス';
$string['collectionaccesseditedsuccessfully'] = 'コレクションが正常に保存されました。';
$string['collectioneditaccess'] = 'あなたには、このコレクション内の %d 件のページに対する編集アクセス権があります';
$string['collectionconfirmdelete'] = 'このコレクション内のページは削除されません。本当にこのコレクションを削除してもよろしいですか?';
$string['collectioncreatedsuccessfully'] = 'コレクションが正常に作成されました。';
$string['collectioncreatedsuccessfullyshare'] = 'あなたのコレクションが正常に作成されました。下記のアクセスリンクを使用して、あなたのコレクションを他のユーザと共有してください。';
$string['collectiondeleted'] = 'コレクションが正常に削除されました。';
$string['collectiondescription'] = 'コレクションは相互にリンクして同一アクセスパーミッションを持つ一連のページです。あなたが好きなだけコレクションを作成することができますが、ページを1つ以上のコレクションに表示することはできません。';
$string['collectiontitle'] = 'コレクションタイトル';
$string['confirmcancelcreatingcollection'] = 'このコレクションは完了していません。本当にキャンセルしてもよろしいですか?';
$string['collectionsaved'] = 'コレクションが正常に保存されました。';
$string['copyacollection'] = 'コレクションをコピーする';
$string['created'] = '作成';
$string['deletecollection'] = 'コレクションを削除する';
$string['deletespecifiedcollection'] = 'コレクション「 %s 」を削除する';
$string['deletingcollection'] = 'コレクションの削除';
$string['deleteview'] = 'コレクションからページを削除する';
$string['description'] = 'コレクション説明';
$string['collectiondragupdate1'] = '「コレクション内のページ」にページを移動するには、「コレクションにページを追加する」ボックスからページ名をドラッグするか、チェックボックスをチェックして「ページを追加する」ボタンをクリックしてください。<br />
あなたはページ名をドラッグまたは矢印ボタンを使用することにより、「コレクション内のページ」エリアのページを並べ替えることができます。';
$string['viewsincollection'] = 'コレクション内のページ';
$string['editcollection'] = 'コレクションを編集する';
$string['editingcollection'] = 'コレクションの編集';
$string['edittitleanddesc'] = 'タイトルおよび説明を編集する';
$string['editviews'] = 'コレクションページを編集する';
$string['editviewaccess'] = 'ページアクセスを編集する';
$string['editaccess'] = 'コレクションアクセスを編集する';
$string['emptycollectionnoeditaccess'] = 'あなたは空のコレクションのアクセスを編集することはできません。最初にページを追加してください。';
$string['emptycollection'] = '空のコレクション';
$string['manageviews'] = 'ページを管理する';
$string['manageviewsspecific'] = '「 %s 」のページを管理する';
$string['name'] = 'コレクション名';
$string['needtoselectaview'] = 'あなたはコレクションに追加するページを選択する必要があります。';
$string['newcollection'] = '新しいコレクション';
$string['nocollections'] = 'まだコレクションはありません。';
$string['nocollectionsaddone'] = 'まだコレクションはありません。%sコレクションを追加してください%s。';
$string['nooverride'] = 'オーバーライドはありません。';
$string['noviewsavailable'] = '追加できるページはありません。';
$string['noviewsaddsome'] = 'コレクションにページはありません。%sページを追加してください%s。';
$string['noviews'] = 'ページはありません。';
$string['overrideaccess'] = 'アクセスをオーバーライドする';
$string['potentialviews'] = '潜在的ページ';
$string['saveapply'] = '適用して保存する';
$string['savecollection'] = 'コレクションを保存する';
$string['update'] = '更新';
$string['usecollectionname'] = 'コレクション名を使用しますか?';
$string['usecollectionnamedesc'] = 'あなたがブロックタイトルの代わりにコレクション名を使用したい場合、このチェックボックスをチェックしてください。';
$string['viewsaddedtocollection1'] = '%s ページがコレクションに追加されました。';
$string['viewsaddedtocollection1different'] = '%s ページがコレクションに追加されました。コレクション内のページすべてに関して、共有アクセスが変更されました。';
$string['viewsaddedaccesschanged'] = '次のページに関して、アクセスパーミッションが変更されました:';
$string['viewcollection'] = 'コレクション詳細';
$string['viewcount'] = 'ページ';
$string['viewremovedsuccessfully'] = 'ページが正常に削除されました。';
$string['viewnavigation'] = 'ページナビゲーションバー';
$string['viewnavigationdesc'] = 'このコレクション内すべてのページに水平ナビゲーションバーをデフォルトで追加します。';
$string['viewstobeadded'] = '追加されるページ';
$string['viewconfirmremove'] = '本当にこのページをコレクションから削除してもよろしいですか?';
$string['collectioncopywouldexceedquota'] = 'このコレクションをコピーすることで、あなたのファイルクオータを超過します。';
$string['copiedpagesblocksandartefactsfromtemplate'] = '%d ページ、%d ブロックおよび %d アーティファクトを %s からコピーしました。';
$string['copiedblogpoststonewjournal'] = 'コピーされた日誌エントリは新しい別の日誌に登録されました。';
$string['by'] = 'by';
$string['copycollection'] = 'コレクションをコピーする';
$string['youhaveonecollection'] = 'あなたには 1 件のコレクションがあります。';
$string['youhavecollections'] = 'あなたには %s 件のコレクションがあります。';
$string['collectionssharedtogroup'] = 'このグループと共有されているコレクション';

?>

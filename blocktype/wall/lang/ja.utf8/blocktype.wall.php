<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2010-09-09 16:46:08 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'ウォール';
$string['otherusertitle'] = '%s のウォール';
$string['description'] = 'ユーザがあなたにコメントを残すことのできるエリアを表示します。';
$string['noposts'] = '表示できるウォール投稿はありません。';
$string['makeyourpostprivate'] = 'あなたの投稿をプライベート (非公開) にしますか?';
$string['viewwall'] = 'ウォールを表示する';
$string['backtoprofile'] = 'プロファイルに戻る';
$string['wall'] = 'ウォール';
$string['wholewall'] = 'すべてのウォールを表示する';
$string['reply'] = '返信';
$string['delete'] = '投稿を削除する';
$string['deletepost'] = '投稿を削除する';
$string['Post'] = '投稿';
$string['deletepostsure'] = '本当にこの処理を実行してもよろしいですか? 元に戻すことはできません。';
$string['deletepostsuccess'] = '投稿が正常に削除されました。';
$string['addpostsuccess'] = '投稿が正常に追加されました。';
$string['maxcharacters'] = '1投稿あたり、最大 %s 文字入力可能です。';
$string['sorrymaxcharacters'] = '申し訳ございません、あなたは %s 文字以上、投稿することができません。';
$string['posttextrequired'] = 'このフィールドは必須入力項目です。';
$string['postsizelimit'] = '投稿サイズ制限';
$string['postsizelimitdescription'] = 'ここであなたはウォールの投稿サイズを制限することができます。既存の投稿に関して、変更されることはありません。';
$string['postsizelimitmaxcharacters'] = '最大文字数';
$string['postsizelimitinvalid'] = 'これは有効な数字ではありません。';
$string['postsizelimittoosmall'] = 'この制限はゼロ以下に設定できません。';

?>

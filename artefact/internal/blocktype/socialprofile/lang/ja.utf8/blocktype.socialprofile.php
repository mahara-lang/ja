<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-10-01 05:06:48 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'ソーシャルプロファイル';
$string['description'] = '表示するソーシャルプロファイルURLを選択する';
$string['profilestoshow'] = '表示するソーシャルプロファイル';
$string['displaysettings'] = '表示設定';
$string['displayaddressesas'] = '次のようにソーシャルプロファイルアドレスを表示する';
$string['optionicononly'] = 'アイコンのみのボタン';
$string['optiontexticon'] = 'アイコンおよびテキストのボタン';
$string['optiontextonly'] = 'テキストのみのボタン';
$string['displaydefaultemail'] = 'ボタンとしてデフォルトメールアドレスのリンクを表示しますか?';
$string['displaymsgservices'] = 'ボタンとしてメッセージングサービスを表示しますか?';

?>

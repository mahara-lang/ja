<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2011-04-20 14:42:30 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = '「グループ」フォーラムの最近の投稿';
$string['description'] = 'グループフォーラムの最近の投稿を表示します。';
$string['group'] = 'グループ';
$string['nogroupstochoosefrom'] = '申し訳ございません、選択できるグループがありません。';
$string['poststoshow'] = '表示する最大投稿数';
$string['poststoshowdescription'] = '設定範囲: 1-100';
$string['recentforumpostsforgroup'] = '%s フォーラムの最近の投稿';
$string['defaulttitledescription'] = 'タイトルフィールドを空白にした場合、デフォルトタイトルが適用されます。';

?>

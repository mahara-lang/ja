<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-09-15 09:35:41 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['findfriends'] = 'フレンドを探す';
$string['mygroups'] = 'マイグループ';
$string['updateprofile'] = 'プロファイルを更新する';
$string['discuss'] = '議論する';
$string['uploadfiles'] = 'ファイルをアップロードする';
$string['createpages'] = 'ページを作成する';
$string['writejournal'] = '日誌エントリを執筆する';
$string['sharepages'] = 'ページを共有する';
$string['logintoexplore'] = 'ログインして探索する!';
$string['clickonactivity'] = '上記活動をクリックしてください。';

?>

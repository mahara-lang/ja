<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2009-01-05 21:12:05 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['emailsubject'] = '%s からのメッセージ: デイリーダイジェスト';
$string['emailbodynoreply'] = 'これは %s から自動的に生成された通知メールです。以下、あなたのすべての通知に関するデイリーダイジェストです。

--------------------------------------------------';
$string['emailbodyending'] = 'あなたの通知プリファレンスを更新するには、%s にアクセスしてください。';
$string['name'] = 'メールダイジェスト';

?>

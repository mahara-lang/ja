<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2012-03-02 15:17:32 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = '最新ページ';
$string['description1'] = 'このサイトにおいて、あなたがアクセスしたページの中から最近更新されたページを一覧表示します。';
$string['viewstoshow'] = '表示するページの最大数';
$string['viewstoshowdescription'] = '設定範囲: 1～100';
$string['defaulttitledescription'] = 'タイトルフィールドを空白にした場合、デフォルトタイトルが適用されます。';

?>

<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-09-02 20:13:21 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['addpostsuccess'] = '投稿が正常に追加されました。';
$string['addtitle'] = 'フォーラムを追加する';
$string['addtopic'] = 'トピックを追加する';
$string['addtopicsuccess'] = 'トピックが正常に追加されました。';
$string['autosubscribeusers'] = 'ユーザを自動的にメール購読させますか?';
$string['autosubscribeusersdescription'] = 'このフォーラムに対して、グループユーザが自動的にメール購読されるかどうか選択してください。';
$string['Body'] = '本文';
$string['cantaddposttoforum'] = 'あなたはこのフォーラムに投稿できません。';
$string['cantaddposttotopic'] = 'あなたはこのトピックに投稿できません。';
$string['cantaddtopic'] = 'あなたはこのフォーラムにトピックを追加できません。';
$string['cantdeletepost'] = 'あなたはこのフォーラムの投稿を削除できません。';
$string['cantdeletethispost'] = 'あなたはこの投稿を削除できません。';
$string['cantdeletetopic'] = 'あなたはこのフォーラムのトピックを削除できません。';
$string['canteditpost'] = 'あなたはこの投稿を編集できません。';
$string['cantedittopic'] = 'あなたはこのトピックを編集できません。';
$string['cantfindforum'] = 'ID %s のフォーラムが見つかりませんでした。';
$string['cantfindpost'] = 'ID %s の投稿が見つかりませんでした。';
$string['cantfindtopic'] = 'ID %s のトピックが見つかりませんでした。';
$string['cantmakenonobjectionable'] = 'あなたはこの投稿を好ましくない投稿としてマークすることはできません。';
$string['cantviewforums'] = 'あなたはこのグループのフォーラムを閲覧できません。';
$string['cantviewtopic'] = 'あなたはこのフォーラムのトピックを閲覧できません。';
$string['chooseanaction'] = '処理を選択する ...';
$string['clicksetsubject'] = '件名を入力するには、ここをクリックしてください。';
$string['Closed'] = 'クローズド';
$string['Close'] = 'クローズ';
$string['closeddescription'] = 'クローズドトピックではモデレータおよびグループ管理者のみ返信することができます。';
$string['complaint'] = 'クレーム';
$string['Count'] = 'カウント';
$string['createtopicusersdescription'] = '「グループメンバーすべて」を設定した場合、すべてのメンバーが新しいトピックを作成すること、既存のトピックに返信することができます。「モデレータおよびグループ管理者のみ」を設定した場合、モデレータおよびグループ管理者のみ、新しいトピックを開始することができます。しかし、「モデレータおよびグループ管理者のみ」を設定して、トピックが存在するようになった場合、すべてのユーザが返信することができます。';
$string['currentmoderators'] = '現在のモデレータ';
$string['defaultforumtitle'] = '一般的なディスカッション';
$string['defaultforumdescription'] = '%s 一般的なディスカッションフォーラム';
$string['deleteforum'] = 'フォーラムを削除する';
$string['deletepost'] = '投稿を削除する';
$string['deletepostsuccess'] = '投稿が正常に削除されました。';
$string['deletepostsure'] = '本当にこの処理を実行してもよろしいですか? 元に戻すことはできません。';
$string['deletetopic'] = 'トピックを削除する';
$string['deletetopicspecific'] = 'トピック「 %s 」を削除する';
$string['deletetopicsuccess'] = 'トピックが正常に削除されました。';
$string['deletetopicsure'] = '本当にこの処理を実行してもよろしいですか? 元に戻すことはできません。';
$string['editpost'] = '投稿を編集する';
$string['editpostsuccess'] = '投稿が正常に編集されました。';
$string['editstothispost'] = 'この投稿に対する編集';
$string['edittitle'] = 'フォーラムを編集する';
$string['edittopic'] = 'トピックを編集する';
$string['edittopicspecific'] = 'トピック「 %s 」を編集する';
$string['edittopicsuccess'] = 'トピックが正常に編集されました。';
$string['forumname'] = 'フォーラム名';
$string['forumposthtmltemplate'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>%s by %s</strong><br>%s</div>

<div style="margin: 1em 0;">%s</div>

<div style="font-size: smaller; border-top: 1px solid #999;">
<p><a href="%s">この投稿にオンライン上で返信する</a></p>
<p><a href="%s">この %s から購読解除する</a></p>
</div>';
$string['forumposttemplate'] = '%s by %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
オンライン上で投稿を閲覧するには、次のリンクにアクセスしてください:
%s

この %s から購読解除するには、次のリンクにアクセスしてください:
%s';
$string['forumsuccessfulsubscribe'] = 'フォーラムのメール購読が正常に登録されました。';
$string['forumsuccessfulunsubscribe'] = 'フォーラムのメール購読が正常に解除されました。';
$string['gotoforums'] = 'フォーラムに移動する';
$string['groupadmins'] = 'グループ管理者';
$string['groupadminlist'] = 'グループ管理者:';
$string['Key'] = 'キー';
$string['lastpost'] = '最新の投稿';
$string['latestforumposts'] = '最新のフォーラム投稿';
$string['Moderators'] = 'モデレータ';
$string['moderatorsandgroupadminsonly'] = 'モデレータおよびグループ管理者のみ';
$string['moderatorslist'] = 'モデレータ:';
$string['moderatorsdescription'] = 'モデレータはトピックおよび投稿を編集および削除することができます。また、オープン、クローズ、トピックのスティッキー設定および設定解除することもできます。';
$string['name'] = 'フォーラム';
$string['nameplural'] = 'フォーラム';
$string['newforum'] = '新しいフォーラム';
$string['newforumpostnotificationsubjectline'] = '%s';
$string['newpost'] = '新しい投稿:';
$string['newtopic'] = '新しいトピック';
$string['noforumpostsyet'] = 'まだこのグループに投稿はありません。';
$string['noforums'] = 'このグループにフォーラムはありません。';
$string['notopics'] = 'このフォーラムにトピックはありません。';
$string['notifyadministrator'] = '管理者に通知する';
$string['objectionablepostdeletedsubject'] = 'フォーラムトピック「 %s 」の好ましくない投稿は %s によって削除されました。';
$string['objectionablepostdeletedbody'] = '%s は前に好ましくないコンテンツであると報告された %s による投稿を調査して削除しました。

好ましくない投稿コンテンツは次のとおりです:
%s';
$string['objectionabletopicdeletedsubject'] = '好ましくないフォーラムトピック「 %s 」は %s によって削除されました。';
$string['objectionabletopicdeletedbody'] = '%s は前に好ましくないコンテンツであると報告された %s によるトピックを調査して削除しました。

好ましくないトピックコンテンツは次のとおりです:
%s';
$string['Open'] = 'オープン';
$string['Order'] = '並び順';
$string['orderdescription'] = '他のフォーラムに対して、あなたがこのフォーラムを配置したい場所を選択してください。';
$string['Post'] = '投稿';
$string['postaftertimeout'] = 'あなたは %s 分のタイムアウト後に変更を送信しました。あなたの変更は適用されません。';
$string['postbyuserwasdeleted'] = '%s による投稿が削除されました。';
$string['postsbyuserweredeleted'] = '%s 件の投稿 (投稿者: %s) が削除されました。';
$string['postdelay'] = '投稿遅延';
$string['postdelaydescription'] = '新しい投稿がメール購読者に送信される前に経過する必要のある最小時間 (分) です。投稿者はこの時間中、投稿内容を編集することができます。';
$string['postedin'] = '%s が %s に投稿しました。';
$string['Poster'] = '投稿者';
$string['postobjectionable'] = 'この投稿はあなたにより好ましくないコンテンツを含んでいると報告されました。';
$string['postnotobjectionable'] = 'この投稿は好ましくないコンテンツを含んでいると報告されました。事実とは異なる場合、あなたはボタンをクリックして通知を削除した後、他の管理者に通知することができます。';
$string['postnotobjectionablebody'] = '%s は前に好ましくないコンテンツであると報告された %s による投稿を調査して、もはや好ましくないコンテンツは含んでいないとマークしました。';
$string['postnotobjectionablesubject'] = 'フォーラムトピック「 %s 」の投稿は %s によって好ましくない投稿ではないとマークされました。';
$string['postnotobjectionablesuccess'] = '投稿が好ましくない投稿としてマークされました。';
$string['postnotobjectionablesubmit'] = '好ましくない投稿ではない';
$string['postreply'] = '返信';
$string['Posts'] = '投稿';
$string['allposts'] = 'すべての投稿';
$string['postsvariable'] = '投稿: %s';
$string['potentialmoderators'] = '潜在的なモデレータ';
$string['re'] = 'Re: %s';
$string['regulartopics'] = '標準トピック';
$string['Reply'] = '返信';
$string['replyforumpostnotificationsubjectline'] = 'Re: %s';
$string['Re:'] = 'Re:';
$string['replyto'] = '返信:';
$string['reporteddetails'] = '報告詳細';
$string['reportedpostdetails'] = '<b>報告 by %s - %s:</b><p>%s</p>';
$string['reportobjectionablematerial'] = '報告';
$string['reportpost'] = '投稿報告';
$string['reportpostsuccess'] = '投稿が正常に報告されました。';
$string['sendnow'] = '今すぐメッセージを送信する';
$string['sendnowdescription'] = '少なくとも %s 分待つのではなく、すぐにメッセージを送信します。';
$string['Sticky'] = 'スティッキー';
$string['stickydescription'] = 'スティッキートピックはすべてのページトップに表示されます。';
$string['stickytopics'] = 'スティッキートピック';
$string['Subscribe'] = 'メール購読';
$string['Subscribed'] = 'メール購読済み';
$string['subscribetoforum'] = 'フォーラムをメール購読する';
$string['subscribetotopic'] = 'トピックをメール購読する';
$string['Subject'] = '件名';
$string['Topic'] = 'トピック';
$string['Topics'] = 'トピック';
$string['topiclower'] = 'トピック';
$string['topicslower'] = 'トピック';
$string['topicclosedsuccess'] = 'トピックが正常にクローズされました。';
$string['topicisclosed'] = 'このトピックはクローズされました。モデレータおよびグループ管理者のみ新しい返信を投稿できます。';
$string['topicopenedsuccess'] = 'トピックが正常にオープンされました。';
$string['topicstickysuccess'] = 'トピックが正常にスティッキー設定されました。';
$string['topicsubscribesuccess'] = 'トピックのメール購読が正常に登録されました。';
$string['topicsuccessfulunsubscribe'] = 'トピックのメール購読が正常に解除されました。';
$string['topicunstickysuccess'] = 'トピックが正常にスティッキー設定解除されました。';
$string['topicunsubscribesuccess'] = 'トピックのメール購読が正常に解除されました。';
$string['topicupdatefailed'] = 'トピックの更新に失敗しました。';
$string['typenewpost'] = '新しいフォーラム投稿';
$string['typereportpost'] = 'フォーラム内の好ましくないコンテンツ';
$string['Unsticky'] = 'スティッキー解除';
$string['Unsubscribe'] = 'メール購読解除';
$string['unsubscribefromforum'] = 'フォーラムのメール購読を解除する';
$string['unsubscribefromtopic'] = 'トピックのメール購読を解除する';
$string['updateselectedtopics'] = '選択したトピックを更新する';
$string['whocancreatetopics'] = '誰がトピックを作成できますか?';
$string['youcannotunsubscribeotherusers'] = 'あなたは他のユーザをメール購読解除できません。';
$string['youarenotsubscribedtothisforum'] = 'あなたはこのフォーラムをメール購読していません。';
$string['youarenotsubscribedtothistopic'] = 'あなたはこのトピックをメール購読していません。';
$string['Moveto'] = '移動';
$string['topicmovedsuccess'] = '%d トピックが正常に移動されました。';
$string['today'] = '今日';
$string['yesterday'] = '昨日';
$string['strftimerecentrelative'] = '%%v - %%H:%%M';
$string['strftimerecentfullrelative'] = '%%v - %%H:%%M';
$string['indentmode'] = 'フォーラムのインデントモード';
$string['indentfullindent'] = '完全に広げる';
$string['indentmaxindent'] = '最大まで広げる';
$string['indentflatindent'] = 'インデントなし';
$string['indentmodedescription'] = 'このフォーラム内のトピックが、どのようにインデントされるか指定してください。';
$string['maxindent'] = '最大インデントレベル';
$string['maxindentdescription'] = 'トピックの最大インデントレベルを設定してください。この設定はインデントモードが「最大インデントレベルまで広げる」に設定された場合のみ適用されます。';
$string['closetopics'] = '新しいトピックをクローズする';
$string['closetopicsdescription'] = 'チェックした場合、このフォーラム内の新しいトピックがデフォルトでクローズされます。モデレータおよびグループ管理者のみ、クローズされたトピックに返信することができます。';
$string['activetopicsdescription'] = 'あなたのグループ内で最近更新されたトピックです。';
$string['timeleftnotice'] = 'あなたが編集を終了するまで %s 分あります。';
$string['objectionablecontentpost'] = 'フォーラムトピック「 %s 」の好ましくないコンテンツ - 報告者: %s';
$string['objectionablecontentposthtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">フォーラムトピック「 %s 」の好ましくないコンテンツ - 報告者:%s
<br />%s</div>

<div style="margin: 1em 0;">%s</div>

<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">好ましくない投稿コンテンツは次のとおりです:
<br>%s</div>

<div style="margin: 1em 0;">%s</div>

<div style="font-size: smaller; border-top: 1px solid #999;">
<p>クレームは次に関連します: <a href="%s">%s</a></p>
<p>報告者: <a href="%s">%s</a></p>
</div>';
$string['objectionablecontentposttext'] = 'フォーラムトピック「 %s 」の好ましくないコンテンツ - 報告者:%s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------

好ましくない投稿コンテンツは次のとおりです:
%s
------------------------------------------------------------------------

%s

-----------------------------------------------------------------------
投稿を閲覧するには、次のリンクにアクセスしてください:
%s
報告者のプロファイルを閲覧するには、次のリンクにアクセスしてください:
%s';

?>

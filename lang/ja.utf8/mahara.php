<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2015-03-12 18:54:11 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['add'] = '追加';
$string['addone'] = '追加';
$string['cancel'] = 'キャンセル';
$string['copy'] = 'コピー';
$string['copytoclipboard'] = 'シークレットURLをクリップボードにコピーする';
$string['delete'] = '削除';
$string['deletespecific'] = '「 %s 」を削除する';
$string['deleteitem'] = '削除';
$string['deleted'] = '削除';
$string['moveitemup'] = '上へ';
$string['moveitemdown'] = '下へ';
$string['edit'] = '編集';
$string['editspecific'] = '「 %s 」を編集する';
$string['editing'] = '編集';
$string['settingsspecific'] = '「 %s 」を設定する';
$string['move'] = '移動';
$string['save'] = '保存';
$string['submit'] = '送信';
$string['update'] = '更新';
$string['change'] = '変更';
$string['send'] = '送信';
$string['go'] = 'Go';
$string['configfor'] = '設定:';
$string['default'] = 'デフォルト';
$string['upload'] = 'アップロード';
$string['complete'] = '完了';
$string['Failed'] = '失敗';
$string['loading'] = 'ローディング ...';
$string['showtags'] = 'マイタグを表示する';
$string['errorprocessingform'] = 'このフォームによる送信中にエラーが発生しました。マークされたフィールドを確認して、再度送信してください。';
$string['description'] = '説明';
$string['remove'] = '削除';
$string['Close'] = '閉じる';
$string['Help'] = 'ヘルプ';
$string['Helpicon'] = 'ヘルプアイコン';
$string['closehelp'] = 'ヘルプを閉じる';
$string['applychanges'] = '変更を適用する';
$string['general'] = '一般';
$string['units'] = 'ユニット';
$string['status'] = 'ステータス';
$string['toggletoolbarson'] = 'ツールバーを有効にします。ボタンの完全リストを表示します。';
$string['toggletoolbarsoff'] = 'ツールバーを無効にします。基本的なボタンを表示します。';
$string['defaulthint'] = '検索用語を入力する';
$string['imagexofy'] = 'イメージ {x} / {y}';
$string['clickformore'] = '(詳細情報を表示するには、「Enter」を押してください)';
$string['goto'] = '「 %s 」に移動する';
$string['gotomore'] = '続きを読む ...';
$string['on'] = 'On';
$string['off'] = 'Off';
$string['yes'] = 'Yes';
$string['no'] = 'No';
$string['true'] = 'True';
$string['false'] = 'False';
$string['enabled'] = '有効';
$string['disabled'] = '無効';
$string['complaint'] = 'クレーム';
$string['notifyadministrator'] = '管理者に通知する';
$string['notifyadministratorconfirm'] = 'このページを好ましくないコンテンツを含んでいるページとして報告しますか?';
$string['notobjectionable'] = '好ましくないコンテンツではない';
$string['reportobjectionablematerial'] = '好ましくないコンテンツを報告する';
$string['reportsent'] = 'あなたの報告が送信されました。';
$string['tabs'] = 'タブ';
$string['tab'] = 'タブ';
$string['selected'] = '選択済み';
$string['admin'] = '管理';
$string['menu'] = 'メニュー';
$string['at'] = '@';
$string['From'] = '開始日';
$string['To'] = '終了日';
$string['ascending'] = '昇順';
$string['descending'] = '降順';
$string['sortorder'] = 'ファイルの並び順';
$string['All'] = 'すべて';
$string['none'] = 'なし';
$string['selectall'] = 'すべてを選択する';
$string['selectnone'] = 'すべての選択を解除する';
$string['enable'] = '有効にする';
$string['disable'] = '無効にする';
$string['show'] = '表示';
$string['hide'] = '非表示';
$string['pluginenabled'] = 'プラグインが有効にされました。';
$string['plugindisabled'] = 'プラグインが無効にされました。';
$string['plugindisableduser'] = '%s プラグインは無効にされています。この機能を有効にするには、あなたの管理者にご相談ください。';
$string['pluginnotenabled'] = 'プラグインが有効にされていません。あなたはまず %s プラグインを有効にする必要があります。';
$string['pluginexplainaddremove'] = 'Maharaプラグインは常にインストールされるため、URLを知っているユーザはアクセスすることができます。機能を有効または無効にする代わりに、以下のプラグインの横にある「表示」または「非表示」リンクをクリックすることにより、プラグインを表示または非表示にすることができます。';
$string['pluginexplainartefactblocktypes'] = '「artefact (アーティファクト)」タイプのプラグインを非表示にした場合、Maharaシステムは同時に関連するブロックの表示を停止します。';
$string['pluginbrokenanddisabledtitle'] = '不完全なプラグイン (%s) が無効にされました。';
$string['pluginbrokenanddisabled'] = 'ユーザが %s プラグインのロードを試みましたが、ロードすることができませんでした。
さらなるエラーを避けるため、このプラグインは無効にされました。

プラグインにより生成されたエラーメッセージは下記のとおりです:
----------------------------------------------------------------------------

%s

----------------------------------------------------------------------------

プラグインを再度有効にするには、あなたのサイトの拡張機能管理ページにアクセスしてください。';
$string['next'] = '次へ';
$string['nextpage'] = '次のページ';
$string['previous'] = '前へ';
$string['prevpage'] = '前のページ';
$string['first'] = '最初';
$string['firstpage'] = '最初のページ';
$string['last'] = '最後';
$string['lastpage'] = '最後のページ';
$string['maxitemsperpage'] = '1ページあたりの最大アイテム数';
$string['accept'] = '承認';
$string['memberofinstitutions'] = '%s のメンバー';
$string['staffofinstitutions'] = '%s のスタッフ';
$string['adminofinstitutions'] = '%s の管理者';
$string['reject'] = '拒否';
$string['sendrequest'] = 'リクエストを送信する';
$string['reason'] = '理由';
$string['select'] = '選択';
$string['tags'] = 'タグ';
$string['tagsdesc'] = 'このアイテムに関して、カンマで区切ったタグを入力してください。';
$string['tagsdescprofile'] = 'このアイテムに関して、カンマで区切ったタグを入力してください。「プロファイル」タグが付けられたアイテムはあなたのサイドバーに表示されます。';
$string['youhavenottaggedanythingyet'] = 'あなたはまたタグ付けしていません。';
$string['mytags'] = 'マイタグ';
$string['Tag'] = 'タグ';
$string['itemstaggedwith'] = '「 %s 」でタグ付けされたアイテム';
$string['nitems'] = '%s アイテム';
$string['searchresultsfor'] = '検索結果:';
$string['alltags'] = 'すべてのタグ';
$string['sortalpha'] = 'タグをアルファベット順でソートする';
$string['sortfreq'] = 'タグを使用頻度順でソートする';
$string['sortresultsby'] = 'ソート結果:';
$string['sortedby'] = '並べ替え';
$string['sortby'] = '並べ替え:';
$string['adminfirst'] = 'Adminを最初に';
$string['nameatoz'] = '名: A -> Z';
$string['nameztoa'] = '名: Z -> A';
$string['firstjoined'] = '登録日昇順';
$string['lastjoined'] = '登録日降順';
$string['date'] = '日時';
$string['earliest'] = '一番古い';
$string['latest'] = '一番新しい';
$string['dateformatguide'] = 'フォーマット「YYYY/MM/DD」を使用してください。';
$string['dateofbirthformatguide'] = 'フォーマット「YYYY/MM/DD」を使用してください。';
$string['datetimeformatguide'] = 'フォーマット「YYYY/MM/DD HH:MM」を使用してください。';
$string['filterresultsby'] = 'フィルタ結果:';
$string['tagfilter_all'] = 'すべて';
$string['tagfilter_file'] = 'ファイル';
$string['tagfilter_image'] = 'イメージ';
$string['tagfilter_text'] = 'テキスト';
$string['tagfilter_view'] = 'ページ';
$string['tagfilter_collection'] = 'コレクション';
$string['edittags'] = 'タグを編集する';
$string['selectatagtoedit'] = '編集するタグを選択してください';
$string['edittag'] = '<a href="%s">%s</a> を編集する';
$string['editthistag'] = 'このタグを編集する';
$string['edittagdescription'] = 'あなたのポートフォリオに関して、「 %s 」でタグ付けされたすべてのアイテムが更新されます。';
$string['deletetag'] = '<a href="%s">%s</a> を削除する';
$string['confirmdeletetag'] = '本当に、あなたのポートフォリオ内すべてのアイテムから、このタグを削除してもよろしいですか?';
$string['deletetagdescription'] = 'あなたのポートフォリオ内すべてのアイテムから、このタグを削除します。';
$string['tagupdatedsuccessfully'] = 'タグが正常に更新されました。';
$string['tagdeletedsuccessfully'] = 'タグが正常に削除されました。';
$string['selfsearch'] = 'マイポートフォリオを検索する';
$string['resultsperpage'] = '1ページあたりの表示ユーザ数';
$string['license'] = 'ライセンス';
$string['licenseother'] = 'その他のライセンス (URLを入力する)';
$string['licenseotherurl'] = 'URLを入力する';
$string['licensedesc'] = 'このコンテンツのライセンスです。';
$string['licensenone'] = '未選択';
$string['licensenonedetailed'] = '%s はこのコンテンツのライセンスを選択していません。';
$string['licensenonedetailedowner'] = 'あなたはこのコンテンツのライセンスを選択していません。';
$string['licensingadvanced'] = 'ライセンス詳細';
$string['licensor'] = 'ライセンサ';
$string['licensordesc'] = 'このコンテンツのライセンサです。';
$string['licensorurl'] = 'オリジナルURL';
$string['licensorurldesc'] = 'このコンテンツのオリジナルURLです。';
$string['licensemandatoryerror'] = 'ライセンスフィールドは必須入力フィールドです。';
$string['licensenocustomerror'] = 'このライセンスはこのサイトで許可されていません。';
$string['quota'] = 'クオータ';
$string['quotausage'] = 'あなたの利用クオータは<span id="quota_used">%s</span> / <span id="quota_total">%s</span> です。';
$string['quotausagegroup'] = 'このグループの利用クオータは<span id="quota_used">%s</span> / <span id="quota_total">%s</span> です。';
$string['groupquota'] = 'グループクオータ';
$string['updatefailed'] = 'アップデートに失敗しました。';
$string['invitedgroup'] = '招待されたグループ';
$string['invitedgroups'] = '招待されたグループ';
$string['logout'] = 'ログアウト';
$string['pendingfriend'] = '保留中のフレンド';
$string['pendingfriends'] = '保留中のフレンド';
$string['profile'] = 'プロファイル';
$string['views'] = 'ページ';
$string['profilecompleteness'] = 'プロファイル完了';
$string['profilecompletenesspreview'] = 'プロファイル完了プレビュー';
$string['profilecompletenesstips'] = 'プロファイル完了ヒント';
$string['progressbargenerictask'] = '追加 %d: %s';
$string['profilecompletionforwhichinstitution'] = '-';
$string['noprogressitems'] = 'このインスティテューションに関して、プロファイル完了アイテムはありません。';
$string['onlineusers'] = 'オンラインユーザ';
$string['lastminutes'] = '直近の %s 分間';
$string['allonline'] = 'すべてのオンラインユーザを表示する';
$string['noonlineusersfound'] = 'オンラインユーザはいません。';
$string['linksandresources'] = 'リンクおよびリソース';
$string['accesstotallydenied_institutionsuspended'] = 'あなたのインスティテューション %s は現在利用停止されています。利用再開されるまで、 %s にログインすることはできません。詳細はあなたのインスティテューションにご連絡ください。';
$string['accessforbiddentoadminsection'] = 'あなたは管理セクションへのアクセスを禁止されています。';
$string['accountdeleted'] = '申し訳ございません、あなたのアカウントは削除されました。<a href="%scontact.php">サイト管理者にご連絡ください</a>。';
$string['accountexpired'] = '申し訳ございません、あなたのアカウントの有効期限が切れました。あなたのアカウントを再度有効にするには、<a href="%scontact.php">サイト管理者にご連絡ください</a>。';
$string['accountcreated'] = '%s: 新しいアカウント';
$string['accountcreatedtext'] = '%s さん

あなたの新しいアカウントが %s に作成されました。あなたの詳細情報は下記のとおりです:

ユーザ名: %s
パスワード: %s

利用開始するには、%s にアクセスしてください!

%s サイト管理者';
$string['accountcreatedchangepasswordtext'] = '%s さん

あなたの新しいアカウントが %s に作成されました。あなたの詳細情報は下記のとおりです:

ユーザ名: %s
パスワード: %s

初回ログイン時、あなたはパスワードの変更を求められます。

利用開始するには、%s にアクセスしてください!

%s サイト管理者';
$string['accountcreatedhtml'] = '<p>%s さん</p>

<p>あなたの新しいアカウントが <a href="%s">%s</a> に作成されました。あなたの詳細情報は下記のとおりです:</p>

<ul>
<li><strong>ユーザ名:</strong> %s</li>
<li><strong>パスワード:</strong> %s</li>
</ul>

<p>利用開始するには、<a href="%s">%s</a>にアクセスしてください!</p>

<p>%s サイト管理者</p>';
$string['accountcreatedchangepasswordhtml'] = '<p>%s さん</p>

<p>あなたの新しいアカウントが <a href="%s">%s</a> に作成されました。あなたの詳細情報は下記のとおりです:</p>

<ul>
<li><strong>ユーザ名:</strong> %s</li>
<li><strong>パスワード:</strong> %s</li>
</ul>

<p>初回ログイン時、あなたはパスワードの変更を求められます。</p>

<p>利用開始するには、<a href="%s">%s</a>にアクセスしてください!</p>

<p>%s サイト管理者</p>';
$string['accountexpirywarning'] = 'アカウント終了通知';
$string['accountexpirywarningtext'] = '%s さん

あなたの %s のアカウントは %s で終了します。

エクスポートツールを使用して、あなたのポートフォリオのコンテンツを保存することをお勧めします。この機能の使用に関するインストラクションはユーザガイドに記載されています。

あなたのアカウントアクセスの有効期限を延長したい場合、または上記内容に関するご質問はお気軽にお問い合わせください:

%s

%s サイト管理者';
$string['accountexpirywarninghtml'] = '<p>%s さん</p>

<p>あなたの %s のアカウントは %s で終了します。</p>

<p>エクスポートツールを使用して、あなたのポートフォリオのコンテンツを保存することをお勧めします。この機能の使用に関するインストラクションはユーザガイドに記載されています。</p>

<p>あなたのアカウントアクセスの有効期限を延長したい場合、または上記内容に関するご質問は<a href="%s">お気軽にお問い合わせください</a>。</P>

<p>%s サイト管理者</p>';
$string['institutionmembershipexpirywarning'] = 'インスティテューションメンバーシップアカウント終了通知';
$string['institutionmembershipexpirywarningtext'] = '%s さん

あなたの %s における %s のメンバーシップは %s で終了します。

あなたのメンバーシップの有効期限を延長したい場合、または上記内容に関するご質問はお気軽にお問い合わせください:

%s

%s サイト管理者';
$string['institutionmembershipexpirywarninghtml'] = '<p>%s さん</p>

<p>あなたの %s における %s のメンバーシップは %s で終了します。</p>

<p>あなたのメンバーシップの有効期限を延長したい場合、または上記内容に関するご質問は<a href="%s">お気軽にお問い合わせください</a>。</P>

<p>%s サイト管理者</p>';
$string['institutionexpirywarning'] = 'インスティテューション終了通知';
$string['institutionexpirywarningtext_institution'] = '%s さん

%s の %s におけるメンバーシップは %s で終了します。

あなたのインスティテューションメンバーシップの有効期限を延長したい場合、または上記内容に関するご質問はお気軽にお問い合わせください:

%s

%s サイト管理者';
$string['institutionexpirywarninghtml_institution'] = '<p>%s さん</p>

<p>%s の %s におけるメンバーシップは %s で終了します。</p>

<p>あなたのインスティテューションメンバーシップの有効期限を延長したい場合、または上記内容に関するご質問は<a href="%s">お気軽にお問い合わせください</a>。</P>

<p>%s サイト管理者</p>';
$string['institutionexpirywarningtext_site'] = '%s さん

インスティテューション「 %a 」は %s で終了します。

%s のメンバーシップの有効期限を延長したい場合、お問い合わせください:

%s サイト管理者';
$string['institutionexpirywarninghtml_site'] = '<p>%s さん</p>

<p>インスティテューション「 %a 」は %s で終了します。</p>

<p>%s のメンバーシップの有効期限を延長したい場合、<a href="%s">お問い合わせください</a>。</P>

<p>%s サイト管理者';
$string['accountinactive'] = '申し訳ございません、あなたのアカウントは現在有効ではありません。';
$string['accountinactivewarning'] = 'アカウント無効通知';
$string['accountinactivewarningtext'] = '%s さん

%s のあなたのアカウントは%s で無効になります。

アカウントが無効になった場合、あなたのアカウントを管理者が再度有効にするまで、ログインすることはできません。

サイトにログインすることで、あなたのアカウントが無効になることを防ぐことができます。

%s サイト管理者';
$string['accountinactivewarninghtml'] = '<p>%s さん</p>

<p>%s のあなたのアカウントは%s で無効になります。</p>

<p>アカウントが無効になった場合、あなたのアカウントを管理者が再度有効にするまで、ログインすることはできません。</p>

<p>サイトにログインすることで、あなたのアカウントが無効になることを防ぐことができます。</p>

<p>%s サイト管理者</p>';
$string['accountsuspended'] = '%s 現在、あなたのアカウントは利用停止されています。<br />あなたの利用停止理由は: %s';
$string['youraccounthasbeensuspended'] = 'あなたのアカウントが利用停止されました。';
$string['youraccounthasbeenunsuspended'] = 'あなたのアカウントの利用停止が解除されました。';
$string['changepasswordinfo'] = '開始する前に、あなたはパスワードを変更する必要があります。';
$string['chooseinstitution'] = 'あなたのインスティテューションを選択する';
$string['chooseusernamepassword'] = 'あなたのユーザ名およびパスワードを選択する';
$string['chooseusernamepasswordinfo'] = 'あなたが %s にログインするには、ユーザ名およびパスワードが必要です。あなたのユーザ名およびパスワードを選択してください。';
$string['confirmpassword'] = 'パスワードをもう一度';
$string['deleteaccount'] = '次のアカウントを削除する: %s / %s';
$string['javascriptnotenabled'] = 'あなたのブラウザではこのサイトに対してJavaスクリプトが有効にされていません。Maharaではあなたがログインする前にJavaスクリプトを有効にする必要があります。';
$string['cookiesnotenabled'] = 'あなたのブラウザではクッキーが有効にされていないか、このサイトからのクッキーがブロックされています。Maharaではあなたがログインする前にクッキーを有効にする必要があります。';
$string['institution'] = 'インスティテューション';
$string['institutioncontacts'] = '「 %s 」連絡先';
$string['institutionlink'] = '<a href="%s">%s</a>';
$string['link'] = '<a href="%s">%s</a>';
$string['loggedoutok'] = 'あなたは正常にログアウトしました。';
$string['login'] = 'ログイン';
$string['loginfailed'] = 'あなたは正しいログイン情報を提供していないようです。あなたのユーザ名およびパスワードが正しいどうか確認してください。';
$string['loginto'] = '%s にログインする';
$string['orloginvia'] = 'または次の認証によりログインする:';
$string['newpassword'] = '新しいパスワード';
$string['nosessionreload'] = 'ログインするには、ページをリロードしてください。';
$string['oldpassword'] = '現在のパスワード';
$string['password'] = 'パスワード';
$string['passwordhelp'] = 'あなたがシステムへのアクセスに使用するパスワードです。';
$string['passwordnotchanged'] = 'あなたはパスワードを変更していません。新しいパスワードを入力してください。';
$string['passwordsaved'] = 'あなたの新しいパスワードが保存されました。';
$string['passwordsdonotmatch'] = 'パスワードが合致しません。';
$string['passwordtooeasy'] = 'あなたのパスワードは簡単すぎます! 難しいパスワードを入力してください。';
$string['register'] = '登録';
$string['sessiontimedout'] = 'あなたのセッションはタイムアウトしました。続けるには再度ログインしてください。';
$string['sessiontimedoutpublic'] = 'あなたのセッションはタイムアウトしました。閲覧を続けるには<a href="%s">ログイン</a>してください。';
$string['sessiontimedoutreload'] = 'あなたのセッションはタイムアウトしました。再度ログインするには、ページをリロードしてください。';
$string['username'] = 'ユーザ名';
$string['preferredname'] = 'ニックネーム';
$string['usernamehelp'] = 'このシステムにアクセスするため、あなたに与えられたユーザ名です。';
$string['youaremasqueradingas'] = 'あなたは %s としてログインしています。';
$string['yournewpassword'] = 'あなたの新しいパスワード';
$string['yournewpasswordagain'] = 'あなたの新しいパスワードをもう一度';
$string['invalidsesskey'] = '無効なセッションキー';
$string['cannotremovedefaultemail'] = 'あなたの主メールアドレスは削除できません。';
$string['emailtoolong'] = 'メールアドレスの長さは半角255文字以内にしてください。';
$string['mustspecifyoldpassword'] = 'あなたの現在のパスワードを入力してください。';
$string['Site'] = 'サイト';
$string['profileicon'] = 'プロファイルイメージ';
$string['bulkselect'] = '編集/報告のためにユーザを選択する';
$string['emailaddress'] = 'メールアドレス';
$string['firstname'] = '名';
$string['firstnameall'] = 'すべての名';
$string['lastname'] = '姓';
$string['lastnameall'] = 'すべての姓';
$string['studentid'] = 'IDナンバー';
$string['displayname'] = '表示名';
$string['fullname'] = 'フルネーム';
$string['registerwelcome'] = 'ようこそ! このサイトを使用するには、最初にユーザ登録してください。';
$string['registeragreeterms'] = 'また、あなたは<a href="terms.php">使用条件</a>に同意する必要があります。';
$string['registerprivacy'] = 'ここで収集したデータは私たちの<a href="privacy.php">プライバシー保護方針</a>に従って保存されます。';
$string['registerstep3fieldsoptional'] = '<h3>任意のプロファイルイメージを選択する</h3><p>あなたは正常に %s にユーザ登録されました! あなたのアバタとして表示する、任意のプロファイル写真を選択することができます。</p>';
$string['registerstep3fieldsmandatory'] = '<h3>必須プロファイルフィールドに入力する</h3><p>以下のフィールドは必須入力フィールドです。あなたのユーザ登録が完了する前に、すべての必須入力フィールドに入力してください。/p>';
$string['registeringdisallowed'] = '申し訳ございません、現在、あなたはこのシステムにユーザ登録できません。';
$string['membershipexpiry'] = 'メンバーシップ有効期限切れ';
$string['institutionfull'] = 'あなたが選択したインスティテューションはこれ以上の登録を受け付けていません。';
$string['registrationnotallowed'] = 'あなたが選択したインスティテューションは自己登録を許可していません。';
$string['registrationcomplete'] = '%s へのご登録ありがとうございます。';
$string['language'] = '言語';
$string['itemdeleted'] = 'アイテムが削除されました。';
$string['itemupdated'] = 'アイテムが更新されました。';
$string['approvalrequired'] = '要承認';
$string['authentication'] = '認証';
$string['cantchangepassword'] = '申し訳ございません、このインターフェース経由でパスワードを変更することはできません - 代わりに、あなたのインスティテューションのインターフェースを使用してください。';
$string['forgotusernamepassword'] = 'あなたのユーザ名またはパスワードを忘れましたか?';
$string['forgotusernamepasswordtextprimaryemail'] = '<p>あなたがユーザ名またはパスワードを忘れた場合、あなたのプロファイルに登録された主メールアドレスを下記に入力してください。あなたが新しいパスワードを作成することのできるメッセージを送信します。</p>
<p>あなたがパスワードを忘れて、ユーザ名を忘れていない場合、ユーザ名を入力することもできます。</p>';
$string['lostusernamepassword'] = 'ユーザ名/パスワード喪失';
$string['emailaddressorusername'] = 'メールアドレスまたはユーザ名';
$string['pwchangerequestsent'] = 'あなたのパスワードを変更するためのリンクを記載したメールが、まもなく送信されます。';
$string['forgotusernamepasswordemailsubject'] = '%s に関するユーザ名/パスワード詳細';
$string['forgotusernamepasswordemailmessagetext'] = '%s さん

あなたの %s のアカウントに関するユーザ名/パスワードのリクエストを受け付けました。

あなたのユーザ名は%s です。

あなたのパスワードをリセットしたい場合、下記のリンクをクリックしてください:

%s

あなたがパスワードのリセットをリクエストしていない場合、このメールは無視してください。

上記内容に関するご質問はお気軽にお問い合わせください:

%s

%s サイト管理者';
$string['forgotusernamepasswordemailmessagehtml'] = '<p>%s さん</p>

<p>あなたの %s のアカウントに関するユーザ名/パスワードのリクエストを受け付けました。</p>

<p>あなたのユーザ名は<strong>%s</strong> です。</p>

<p>あなたのパスワードをリセットしたい場合、下記のリンクをクリックしてください:</p>

<p><a href="%s">%s</a></p>

<p>あなたがパスワードのリセットをリクエストしていない場合、このメールは無視してください。</p>

<p>上記内容に関するご質問は<a href="%s">お気軽にお問い合わせください</a>。</p>

<p>%s サイト管理者</p>';
$string['forgotpassemailsendunsuccessful'] = '申し訳ございません、メールを正常に送信できませんでした。これは私たちに責任があります。もう一度お試しください。';
$string['forgotpassemailsentanyway1'] = 'このユーザに関して保存されているメールアドレス宛にメールが送信されましたが、メールアドレスが正しくない、または受信サーバがメッセージを戻しました。メールを受信していない場合、あなたのパスワードをリセットするために %s 管理者にご連絡ください。';
$string['forgotpassnosuchemailaddressorusername'] = 'あなたが入力したメールアドレスはこのサイトのユーザに合致しません。';
$string['forgotpassuserusingexternalauthentication'] = 'あなたは外部認証方法の使用をリクエストしました。パスワードを変更するには、<a href="%s">あなたの管理者にご連絡ください</a>。または、別のユーザ名またはメールドレスを入力してください。';
$string['forgotpasswordenternew'] = '続けるにはあなたの新しいパスワードを入力してください。';
$string['nosuchpasswordrequest'] = 'そのようなパスワードリクエストはありません。';
$string['passwordresetexpired'] = 'パスワードリセットキーの有効期限が切れました。';
$string['passwordchangedok'] = 'あなたのパスワードが正常に変更されました。';
$string['noinstitutionsetpassemailsubject'] = '%s: %s のメンバーシップ';
$string['noinstitutionsetpassemailmessagetext'] = '%s さん

あなたは %s のメンバーではなくなりました。
続けて %s を現在のユーザ名 %s で使用することができますが、アカウントに新しいパスワードを設定する必要があります。

パスワードリセット処理を続けるには下記リンクをクリックしてください。

%sforgotpass.php?key=%s

上記内容に関するご質問はお気軽にお問い合わせください。

%scontact.php

%s サイト管理者

%sforgotpass.php?key=%s';
$string['noinstitutionsetpassemailmessagehtml'] = '<p>%s さん</p>

<p>あなたは %s のメンバーではなくなりました。</p>
<p>続けて %s を現在のユーザ名 %s で使用することができますが、アカウントに新しいパスワードを設定する必要があります。</p>

<p>パスワードリセット処理を続けるには下記リンクをクリックしてください。</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>

<p>上記内容に関するご質問は<a href="%scontact.php">お気軽にお問い合わせください</a>。</p>

<p>%s サイト管理者</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>';
$string['noinstitutionoldpassemailsubject'] = '%s: %s のメンバーシップ';
$string['noinstitutionoldpassemailmessagetext'] = '%s さん

あなたは %s のメンバーではなくなりました。
あなたは続けて %s を現在のユーザ名 %s およびアカウントに設定したパスワードを使用することができます。

あなたのパスワードを忘れてしまった場合、以下のページにアクセスして、あなたのユーザ名を入力することでパスワードをリセットすることができます。

%sforgotpass.php

上記内容に関するご質問はお気軽にお問い合わせください。

%scontact.php

%s サイト管理者

%sforgotpass.php';
$string['noinstitutionoldpassemailmessagehtml'] = '<p>%s さん</p>

<p>あなたは %s のメンバーではなくなりました。</p>
<p>あなたは続けて %s を現在のユーザ名 %s およびアカウントに設定したパスワードを使用することができます。</p>

<p>パスワードを忘れてしまった場合、以下のページにアクセスして、あなたのユーザ名を入力することでパスワードをリセットすることができます。</p>

<p><a href="%sforgotpass.php">%sforgotpass.php</a></p>

<p>上記内容に関するご質問はお気軽に<a href="%scontact.php">お問い合わせください</a>。</p>。

<p>%s サイト管理者</p>

<p><a href="%sforgotpass.php">%sforgotpass.php</a></p>';
$string['debugemail'] = '注意: このメールは%s <%s> 宛のメールですが、設定ファイルの「sendallemailto」設定により、あなたに送信されています。';
$string['divertingemailto'] = 'メールを %s に転送する';
$string['noenddate'] = '終了日なし';
$string['day'] = '日';
$string['days'] = '日';
$string['weeks'] = '週';
$string['month'] = '月';
$string['months'] = '月';
$string['years'] = '年';
$string['year'] = '年';
$string['datepicker_clearText'] = 'クリア';
$string['datepicker_closeText'] = '完了';
$string['datepicker_closeStatus'] = '変更せずに保存する';
$string['datepicker_prevText'] = '前へ';
$string['datepicker_prevStatus'] = '前の月を表示する';
$string['datepicker_nextText'] = '次へ';
$string['datepicker_nextStatus'] = '次の月を表示する';
$string['datepicker_currentText'] = '現在';
$string['datepicker_currentStatus'] = '現在の月を表示する';
$string['datepicker_monthNames'] = '[1月,2月,3月,4月,5月,6月,7月,8月,9月,10月,11月,12月]';
$string['datepicker_monthNamesShort'] = '[1月,2月,3月,4月,5月,6月,7月,8月,9月,10月,11月,12月]';
$string['datepicker_monthStatus'] = '異なる月を表示する';
$string['datepicker_yearStatus'] = '別の年を表示する';
$string['datepicker_weekHeader'] = '週';
$string['datepicker_dayNames'] = '[日曜日,月曜日,火曜日,水曜日,木曜日,金曜日,土曜日]';
$string['datepicker_dayNamesShort'] = '[日,月,火,水,木,金,土]';
$string['datepicker_dayNamesMin'] = '[日,月,火,水,木,金,土]';
$string['datepicker_dayStatus'] = 'DDを週の最初の日として使用する';
$string['datepicker_dateStatus'] = 'DD, MM d, yyを選択する';
$string['datepicker_initStatus'] = '日付を選択する';
$string['datepicker_timeOnlyTitle'] = '時間を選択する';
$string['datepicker_timeText'] = '時間';
$string['datepicker_hourText'] = '時';
$string['datepicker_minuteText'] = '分';
$string['datepicker_secondText'] = '秒';
$string['datepicker_millisecText'] = 'ミリ秒';
$string['datepicker_timezoneText'] = 'タイムゾーン';
$string['datepicker_amNames'] = '[AM, A]';
$string['datepicker_pmNames'] = '[PM, P]';
$string['sitecontentnotfound'] = '%s テキストは利用できません。';
$string['name'] = '名称';
$string['email'] = 'Eメール';
$string['emails'] = 'Eメール';
$string['subject'] = '件名';
$string['message'] = 'メッセージ';
$string['messageoptional'] = 'メッセージ <span class="accessible-hidden">(任意)</span>';
$string['messagesent'] = 'あなたのメッセージが送信されました。';
$string['nosendernamefound'] = '送信者名が送信されていません。';
$string['emailnotsent'] = 'コンタクトメールの送信に失敗しました。エラーメッセージ: %s';
$string['namedfieldempty'] = '必須入力フィールド「 %s 」が空白です。';
$string['processing'] = '処理中';
$string['unknownerror'] = '不明なエラーが発生しました (0x20f91a0)。';
$string['skipmenu'] = 'メインコンテンツにスキップする';
$string['dropdownmenu'] = 'メニュー';
$string['overview'] = '概要';
$string['home'] = 'ホーム';
$string['Content'] = 'コンテンツ';
$string['myportfolio'] = 'マイポートフォリオ';
$string['settings'] = '設定';
$string['myfriends'] = 'マイフレンド';
$string['findfriends'] = 'フレンドを探す';
$string['groups'] = 'グループ';
$string['mygroups'] = 'マイグループ';
$string['findgroups'] = 'グループを探す';
$string['returntosite'] = 'サイトに戻る';
$string['administration'] = '管理';
$string['siteinformation'] = 'サイト情報';
$string['institutioninformation'] = 'インスティテューション情報';
$string['unreadmessages'] = '未読メッセージ';
$string['unreadmessage'] = '未読メッセージ';
$string['siteclosed'] = 'データベースアップグレードのため、サイトは一時的に閉鎖されています。サイト管理者はログインすることができます。';
$string['siteclosedlogindisabled'] = 'データベースアップグレードのため、サイトは一時的に閉鎖されています。<a href="%s">今からアップグレードを実行します。</a>';
$string['termsandconditions'] = '使用条件';
$string['privacystatement'] = 'プライバシー保護方針';
$string['about'] = 'About';
$string['contactus'] = 'お問い合わせ';
$string['account'] = 'マイアカウント';
$string['accountprefs'] = 'プリファレンス';
$string['preferences'] = 'プリファレンス';
$string['activityprefs'] = '活動プリファレンス';
$string['changepassword'] = 'パスワードを変更する';
$string['notifications'] = '通知';
$string['inbox'] = '受信箱';
$string['gotoinbox'] = '受信箱に移動する';
$string['institutionmembership'] = 'インスティテューションメンバーシップ';
$string['institutionmembershipdescription'] = 'ここにはあなたがメンバーとなっているインスティテューションが表示されます。あなたはインスティテューションのメンバーシップをリクエストすることもできます。また、インスティテューションがあなたに参加招待した場合、あなたは招待を承認または拒否することができます。';
$string['youareamemberof'] = 'あなたは　%s のメンバーです。';
$string['leaveinstitution'] = 'インスティテューションを離れる';
$string['reallyleaveinstitution'] = '本当にこのインスティテューションから離れてもよろしいですか?';
$string['youhaverequestedmembershipof'] = 'あなたは %s のメンバーシップをリクエストしました。';
$string['cancelrequest'] = 'リクエストをキャンセルする';
$string['youhavebeeninvitedtojoin'] = 'あなたは %s への参加を招待されました。';
$string['confirminvitation'] = '招待を承認する';
$string['joininstitution'] = 'インスティテューションに参加する';
$string['decline'] = '拒否';
$string['requestmembershipofaninstitution'] = 'インスティテューションメンバーシップのリクエスト';
$string['optionalinstitutionid'] = 'インスティテューションID';
$string['institutionmemberconfirmsubject'] = 'インスティテューションメンバーシップの承認';
$string['institutionmemberconfirmmessage'] = 'あなたは %s にメンバーとして追加されました。';
$string['institutionmemberrejectsubject'] = 'インスティテューションメンバーシップのリクエストが拒否されました。';
$string['institutionmemberrejectmessage'] = 'あなたの %s に対するメンバーシップのリクエストは拒否されました。';
$string['noinstitutionstafffound'] = 'インスティテューションスタッフは登録されていません。';
$string['noinstitutionadminfound'] = 'インスティテューション管理者は登録されていません。';
$string['Memberships'] = 'メンバーシップ';
$string['Requests'] = 'リクエスト';
$string['Invitations'] = '招待';
$string['institutionmembershipfullsubject'] = 'インスティテューションメンバーシップ定員到達';
$string['institutionmembershipfullmessagetext'] = '%s さん

%s - %s の最大ユーザ数に到達しました。

既存のユーザアカウントを整理するか、このインスティテューションに関する最大ユーザアカウント数の増加をご依頼ください。すべてのサイト管理者は制限を増加させることができます。

--
%s チーム';
$string['config'] = '設定';
$string['sendmessage'] = 'メッセージを送信する';
$string['spamtrap'] = 'スパムトラップ';
$string['formerror'] = 'あなたの送信の処理中にエラーが発生しました。再度お試しください。';
$string['formerroremail'] = 'あなたの問題が続いている場合、%s より私たちにご連絡ください。';
$string['blacklisteddomaininurl'] = 'このフィールドにはブラックリストに登録されたドメイン %s のURLが含まれています。';
$string['newuserscantpostlinksorimages'] = '申し訳ございません、新しく登録されたユーザによるリンクの投稿は許可されていません。リンクまたはURLを取り除くため、あなたの投稿を書き換えて、再度お試しください。';
$string['notinstallable'] = 'インストールできません!';
$string['installedplugins'] = 'インストール済みプラグイン';
$string['notinstalledplugins'] = '<span class="error">未インストールプラグイン</span>';
$string['plugintype'] = 'プラグインタイプ';
$string['settingssaved'] = '設定が保存されました。';
$string['settingssavefailed'] = '設定の保存に失敗しました。';
$string['width'] = '幅';
$string['height'] = '高さ';
$string['widthshort'] = 'w';
$string['heightshort'] = 'h';
$string['filter'] = 'フィルタ';
$string['expand'] = '広げる';
$string['collapse'] = '折りたたむ';
$string['more...'] = 'さらに ...';
$string['nohelpfound'] = 'このアイテムのヘルプファイルはありません。';
$string['nohelpfoundpage'] = 'このページのヘルプファイルはありません。';
$string['couldnotgethelp'] = 'ヘルプページの検索中にエラーが発生しました。';
$string['profileimagetext'] = '%s のプロファイル写真';
$string['profileimagetextanonymous'] = '匿名プロファイル写真';
$string['primaryemailinvalid'] = 'あなたの主メールアドレスが有効ではありません。';
$string['addemail'] = 'メールアドレスを追加する';
$string['search'] = '検索';
$string['searchtype'] = '検索タイプ';
$string['searchusers'] = 'ユーザを検索する';
$string['Query'] = 'クエリ';
$string['query'] = 'クエリ';
$string['querydescription'] = '検索語';
$string['result'] = '結果';
$string['results'] = '結果';
$string['Results'] = '結果';
$string['noresultsfound'] = '該当データはありません。';
$string['users'] = 'ユーザ';
$string['searchwithin'] = '検索対象';
$string['artefact'] = 'アーティファクト';
$string['Artefact'] = 'アーティファクト';
$string['Artefacts'] = 'アーティファクト';
$string['artefactnotfound'] = 'ID %s のアーティファクトは見つかりませんでした。';
$string['artefactnotrendered'] = 'アーティファクトが提出されていません。';
$string['nodeletepermission'] = 'あなたにはこのアーティファクトを削除するパーミッションがありません。';
$string['noeditpermission'] = 'あなたにはこのアーティファクトを編集するパーミッションがありません。';
$string['cantbedeleted'] = 'このアーティファクトまたはサブアーティファクトが送信済みページにあるため、このアーティファクトを削除することはできません。';
$string['Permissions'] = 'パーミッション';
$string['republish'] = '公開';
$string['view'] = 'ページ';
$string['artefactnotpublishable'] = 'アーティファクト %s はページ %s で公開できません。';
$string['nopublishpermissiononartefact'] = 'あなたには %s を公開するパーミッションがありません。';
$string['nopathfound'] = 'このアーティファクトのパスが見つかりませんでした。';
$string['cantmoveitem'] = 'このアーティファクトを移動できません。';
$string['belongingto'] = '所属';
$string['allusers'] = 'すべてのユーザ';
$string['attachment'] = '添付ファイル';
$string['editaccess'] = 'アクセスを編集する';
$string['quarantinedirname'] = '隔離場所';
$string['clammovedfile'] = 'ファイルが隔離場所に移動されました。';
$string['clamdeletedfile'] = 'ファイルが削除されました。';
$string['clamdeletedfilefailed'] = 'ファイルを削除できませんでした。';
$string['clambroken'] = 'あなたの管理者がファイルのアップロードに関して、ウイルスチェックを有効にしましたが、設定が正しくないようです。あなたのファイルは正常にアップロードされませんでした。あなたの管理者にメール通知が送信されましたので、管理者は問題を修正することができます。このファイルのアップロードを後でお試しください。';
$string['clamemailsubject'] = '%s :: Clam AV通知';
$string['clamlost'] = 'Clam AVがファイルのアップロード時に動作するよう設定されていますが、Clam AVのパス「 %s 」が正しくありません。';
$string['clamnotset'] = 'あなたはウイルスチェックを有効にしましたが、「ClamAVのパス」を設定していません。あなたのconfig.phpファイルに「$cfg->pathtoclam」を追加してClamAVのパスを設定するまで、ウイルスチェックは有効になりません。';
$string['clamfailed'] = 'Clam AVの動作にエラーが発生しました。エラーメッセージは%s です。Clam AVのアウトプットは次のとおりです:';
$string['clamunknownerror'] = 'clamに不明なエラーが発生しました。';
$string['image'] = 'イメージ';
$string['filenotimage'] = 'あなたがアップロードしたファイルは有効なイメージではありません。PNG、JPEGまたはGIFファイルをアップロードしてください。';
$string['uploadedfiletoobig'] = 'ファイルが大き過ぎます。詳細はあなたの管理者にお尋ねください。';
$string['notphpuploadedfile'] = 'アップロード処理中にファイルを喪失しました。これは通常起きることではありません。詳細はあなたの管理者にご連絡ください。';
$string['virusfounduser'] = 'あなたがアップロードした %s にウイルスの感染が発見されました! あなたのファイルは正常にアップロードされませんでした。';
$string['fileunknowntype'] = 'あなたがアップロードしたファイルのタイプを判定できませんでした。あなたのファイルが破損したか、設定に問題があります。あなたの管理者にご連絡ください。';
$string['virusrepeatsubject'] = '警告: %s はウイルスの繰り返しアップロード者です。';
$string['virusrepeatmessage'] = 'ユーザ %s がアップロードした複数のファイルにウイルスが感染しています。';
$string['exportfiletoobig'] = '生成されようとしているファイルが大きすぎます。ディスクスペースを空けてください。';
$string['phpuploaderror'] = 'ファイルアップロード中にエラーが発生しました: %s (エラーコード %s)';
$string['phpuploaderror_1'] = 'アップロードファイルがphp.iniのupload_max_filesizeディレクティブを超過しました。';
$string['phpuploaderror_2'] = 'アップロードファイルがHTMLフォームで指定されているMAX_FILE_SIZEディレクティブを超過しました。';
$string['phpuploaderror_3'] = 'アップロードファイルは一部分のみアップロードされました。';
$string['phpuploaderror_4'] = 'アップロードされたファイルはありません。';
$string['phpuploaderror_6'] = '一時フォルダがありません。';
$string['phpuploaderror_7'] = 'ファイルのディスク書き込みに失敗しました。';
$string['phpuploaderror_8'] = 'ファイルアップロードは拡張子が原因で中止されました。';
$string['adminphpuploaderror'] = 'ファイルアップロードエラーは恐らくあなたのサーバ設定を原因としています。';
$string['youraccounthasbeensuspendedtext2'] = 'あなたの %s におけるアカウントが %s によって利用停止されました。';
$string['youraccounthasbeensuspendedreasontext'] = 'あなたの %s におけるアカウントが %s によって利用停止されました。利用停止の理由は:';
$string['youraccounthasbeenunsuspendedtext2'] = 'あなたの %s におけるアカウントの利用停止が解除されました。あなたは再度このサイトにログインして利用することができます。';
$string['sizemb'] = 'MB';
$string['sizekb'] = 'KB';
$string['sizegb'] = 'GB';
$string['sizeb'] = 'b';
$string['bytes'] = 'バイト';
$string['country.af'] = 'アフガニスタン・イスラム国';
$string['country.ax'] = 'オーランド諸島';
$string['country.al'] = 'アルバニア共和国';
$string['country.dz'] = 'アルジェリア民主人民共和国';
$string['country.as'] = '米国領サモア';
$string['country.ad'] = 'アンドラ公国';
$string['country.ao'] = 'アンゴラ共和国';
$string['country.ai'] = 'アンギラ';
$string['country.aq'] = '南極地域';
$string['country.ag'] = 'アンティグア・バーブーダ';
$string['country.ar'] = 'アルゼンチン共和国';
$string['country.am'] = 'アルメニア共和国';
$string['country.aw'] = 'アルバ';
$string['country.au'] = 'オーストラリア';
$string['country.at'] = 'オーストリア共和国';
$string['country.az'] = 'アゼルバイジャン共和国';
$string['country.bs'] = 'バハマ国';
$string['country.bh'] = 'バーレーン国';
$string['country.bd'] = 'バングラデシュ人民共和国';
$string['country.bb'] = 'バルバドス';
$string['country.by'] = 'ベラルーシ';
$string['country.be'] = 'ベルギー王国';
$string['country.bz'] = 'ベリーズ';
$string['country.bj'] = 'ベナン共和国';
$string['country.bm'] = 'バーミューダ諸島';
$string['country.bt'] = 'ブータン王国';
$string['country.bo'] = 'ボリビア共和国';
$string['country.ba'] = 'ボスニア・ヘルツェゴビナ共和国';
$string['country.bw'] = 'ボツワナ共和国';
$string['country.bv'] = 'ブーベ島';
$string['country.br'] = 'ブラジル';
$string['country.io'] = '英国領インド洋地域';
$string['country.bn'] = 'ブルネイ・ダルサラーム国';
$string['country.bg'] = 'ブルガリア共和国';
$string['country.bf'] = 'ブルキナファソ';
$string['country.bi'] = 'ブルンジ共和国';
$string['country.kh'] = 'カンボジア王国';
$string['country.cm'] = 'カメルーン共和国';
$string['country.ca'] = 'カナダ';
$string['country.cv'] = 'カーボベルデ共和国';
$string['country.ky'] = 'ケイマン諸島';
$string['country.cf'] = '中央アフリカ共和国';
$string['country.td'] = 'チャド共和国';
$string['country.cl'] = 'チリ共和国';
$string['country.cn'] = '中華人民共和国';
$string['country.cx'] = 'クリスマス島';
$string['country.cc'] = 'ココス諸島';
$string['country.co'] = 'コロンビア共和国';
$string['country.km'] = 'コモロ・イスラム連邦共和国';
$string['country.cg'] = 'コンゴ共和国';
$string['country.cd'] = 'コンゴ民主共和国';
$string['country.ck'] = 'クック諸島';
$string['country.cr'] = 'コスタリカ共和国';
$string['country.ci'] = 'コートジボワール共和国';
$string['country.hr'] = 'クロアチア共和国';
$string['country.cu'] = 'キューバ共和国';
$string['country.cy'] = 'キプロス共和国';
$string['country.cz'] = 'チェコ共和国';
$string['country.dk'] = 'デンマーク王国';
$string['country.dj'] = 'ジブチ共和国';
$string['country.dm'] = 'ドミニカ国';
$string['country.do'] = 'ドミニカ共和国';
$string['country.ec'] = 'エクアドル共和国';
$string['country.eg'] = 'エジプト・アラブ共和国';
$string['country.sv'] = 'エルサルバドル共和国';
$string['country.gq'] = '赤道ギニア共和国';
$string['country.er'] = 'エリトリア国';
$string['country.ee'] = 'エストニア共和国';
$string['country.et'] = 'エチオピア連邦民主共和国';
$string['country.fk'] = 'フォークランド (マルビナス) 諸島';
$string['country.fo'] = 'フェロー諸島';
$string['country.fj'] = 'フィジー共和国';
$string['country.fi'] = 'フィンランド共和国';
$string['country.fr'] = 'フランス共和国';
$string['country.gf'] = 'フランス領ギアナ';
$string['country.pf'] = 'フランス領ポリネシア';
$string['country.tf'] = 'フランス領極南諸島';
$string['country.ga'] = 'ガボン共和国';
$string['country.gm'] = 'ガンビア共和国';
$string['country.ge'] = 'グルジア共和国';
$string['country.de'] = 'ドイツ連邦共和国';
$string['country.gh'] = 'ガーナ共和国';
$string['country.gi'] = 'ジブラルタル';
$string['country.gr'] = 'ギリシャ共和国';
$string['country.gl'] = 'グリーンランド';
$string['country.gd'] = 'グレナダ';
$string['country.gp'] = 'グアドループ島';
$string['country.gu'] = 'グァム';
$string['country.gt'] = 'グアテマラ共和国';
$string['country.gg'] = 'ガーンジー島';
$string['country.gn'] = 'ギニア共和国';
$string['country.gw'] = 'ギニアビサウ共和国';
$string['country.gy'] = 'ガイアナ協同共和国';
$string['country.ht'] = 'ハイチ共和国';
$string['country.hm'] = 'ヘアド島・マクドナルド諸島';
$string['country.va'] = 'バチカン市国';
$string['country.hn'] = 'ホンジュラス共和国';
$string['country.hk'] = '香港';
$string['country.hu'] = 'ハンガリー共和国';
$string['country.is'] = 'アイスランド共和国';
$string['country.in'] = 'インド';
$string['country.id'] = 'インドネシア共和国';
$string['country.ir'] = 'イラン・イスラム共和国';
$string['country.iq'] = 'イラク共和国';
$string['country.ie'] = 'アイルランド';
$string['country.im'] = 'マン島';
$string['country.il'] = 'イスラエル国';
$string['country.it'] = 'イタリア共和国';
$string['country.jm'] = 'ジャマイカ';
$string['country.jp'] = '日本';
$string['country.je'] = 'ジャージー島';
$string['country.jo'] = 'ヨルダン・ハシミテ王国';
$string['country.kz'] = 'カザフスタン共和国';
$string['country.ke'] = 'ケニア共和国';
$string['country.ki'] = 'キリバス共和国';
$string['country.kp'] = '朝鮮民主主義人民共和国';
$string['country.kr'] = '大韓民国';
$string['country.kw'] = 'クウェート国';
$string['country.kg'] = 'キルギスタン共和国';
$string['country.la'] = 'ラオス人民民主共和国';
$string['country.lv'] = 'ラトビア共和国';
$string['country.lb'] = 'レバノン共和国';
$string['country.ls'] = 'レソト王国';
$string['country.lr'] = 'リベリア共和国';
$string['country.ly'] = '社会主義人民リビア・アラブ国';
$string['country.li'] = 'リヒテンシュタイン公国';
$string['country.lt'] = 'リトアニア共和国';
$string['country.lu'] = 'ルクセンブルク大公国';
$string['country.mo'] = 'マカオ';
$string['country.mk'] = 'マケドニア・旧ユーゴスラビア共和国';
$string['country.mg'] = 'マダガスカル共和国';
$string['country.mw'] = 'マラウイ共和国';
$string['country.my'] = 'マレーシア';
$string['country.mv'] = 'モルディヴ共和国';
$string['country.ml'] = 'マリ共和国';
$string['country.mt'] = 'マルタ共和国';
$string['country.mh'] = 'マーシャル諸島共和国';
$string['country.mq'] = 'マルチニーク島（フランス統治）';
$string['country.mr'] = 'モーリタニア・イスラム共和国';
$string['country.mu'] = 'モーリシャス共和国';
$string['country.yt'] = 'マイヨット島';
$string['country.mx'] = 'メキシコ合衆国';
$string['country.fm'] = 'ミクロネシア連邦';
$string['country.md'] = 'モルドバ共和国';
$string['country.mc'] = 'モナコ公国';
$string['country.mn'] = 'モンゴル国';
$string['country.ms'] = 'モントセラト（英国統治）';
$string['country.ma'] = 'モロッコ王国';
$string['country.mz'] = 'モザンビーク共和国';
$string['country.mm'] = 'ミャンマー連邦';
$string['country.na'] = 'ナミビア共和国';
$string['country.nr'] = 'ナウル共和国';
$string['country.np'] = 'ネパール王国';
$string['country.nl'] = 'オランダ王国';
$string['country.an'] = 'オランダ領アンチル';
$string['country.nc'] = 'ニューカレドニア';
$string['country.nz'] = 'ニュージーランド';
$string['country.ni'] = 'ニカラグア共和国';
$string['country.ne'] = 'ニジェール共和国';
$string['country.ng'] = 'ナイジェリア連邦共和国';
$string['country.nu'] = 'ニウエ';
$string['country.nf'] = 'ノーフォーク島';
$string['country.mp'] = '北マリアナ諸島';
$string['country.no'] = 'ノルウェー王国';
$string['country.om'] = 'オマーン国';
$string['country.pk'] = 'パキスタン・イスラム共和国';
$string['country.pw'] = 'パラオ共和国';
$string['country.ps'] = 'パレスチナ王国';
$string['country.pa'] = 'パナマ共和国';
$string['country.pg'] = 'パプアニューギニア';
$string['country.py'] = 'パラグアイ共和国';
$string['country.pe'] = 'ペルー共和国';
$string['country.ph'] = 'フィリピン共和国';
$string['country.pn'] = 'ピトケアン島';
$string['country.pl'] = 'ポーランド共和国';
$string['country.pt'] = 'ポルトガル共和国';
$string['country.pr'] = 'プエルトリコ';
$string['country.qa'] = 'カタール国';
$string['country.re'] = 'レユニオン';
$string['country.ro'] = 'ルーマニア';
$string['country.ru'] = 'ロシア連邦';
$string['country.rw'] = 'ルワンダ共和国';
$string['country.sh'] = 'セントヘレナ島';
$string['country.kn'] = 'セントクリストファー・ネイビス';
$string['country.lc'] = 'セントルシア';
$string['country.pm'] = 'サンピェール島・ミクロン島';
$string['country.vc'] = 'セントビンセントおよびグレナディーン諸島';
$string['country.ws'] = '西サモア';
$string['country.sm'] = 'サンマリノ共和国';
$string['country.st'] = 'サントメ・プリンシペ民主共和国';
$string['country.sa'] = 'サウジアラビア王国';
$string['country.sn'] = 'セネガル共和国';
$string['country.cs'] = 'セルビア・モンテネグロ';
$string['country.sc'] = 'セイシェル共和国';
$string['country.sl'] = 'シエラレオネ共和国';
$string['country.sg'] = 'シンガポール共和国';
$string['country.sk'] = 'スロバキア共和国';
$string['country.si'] = 'スロベニア共和国';
$string['country.sb'] = 'ソロモン諸島';
$string['country.so'] = 'ソマリア民主共和国';
$string['country.za'] = '南アフリカ共和国';
$string['country.gs'] = 'サウスジョージア・サウスサンドウィッチ諸島';
$string['country.es'] = 'スペイン';
$string['country.lk'] = 'スリランカ民主社会主義共和国';
$string['country.sd'] = 'スーダン共和国';
$string['country.sr'] = 'スリナム共和国';
$string['country.sj'] = 'スバールバル諸島・ヤンマイエン島';
$string['country.sz'] = 'スワジランド王国';
$string['country.se'] = 'スウェーデン王国';
$string['country.ch'] = 'スイス連邦';
$string['country.sy'] = 'シリア・アラブ共和国';
$string['country.tw'] = '台湾';
$string['country.tj'] = 'タジキスタン共和国';
$string['country.tz'] = 'タンザニア連合共和国';
$string['country.th'] = 'タイ王国';
$string['country.tl'] = '東ティモール民主共和国';
$string['country.tg'] = 'タイ王国';
$string['country.tk'] = 'トケラウ諸島';
$string['country.to'] = 'トンガ王国';
$string['country.tt'] = 'トリニダード・トバゴ共和国';
$string['country.tn'] = 'チュニジア共和国';
$string['country.tr'] = 'トルコ共和国';
$string['country.tm'] = 'トルクメニスタン';
$string['country.tc'] = 'タークス諸島・カイコス諸島';
$string['country.tv'] = 'ツバル';
$string['country.ug'] = 'ウガンダ共和国';
$string['country.ua'] = 'ウクライナ';
$string['country.ae'] = 'アラブ首長国連邦';
$string['country.gb'] = '英国';
$string['country.us'] = 'アメリカ合衆国';
$string['country.um'] = 'アメリカ合衆国外諸島';
$string['country.uy'] = 'ウルグアイ東方共和国';
$string['country.uz'] = 'ウズベキスタン共和国';
$string['country.vu'] = 'バヌアツ共和国';
$string['country.ve'] = 'ベネズエラ共和国';
$string['country.vn'] = 'ベトナム社会主義共和国';
$string['country.vg'] = '英国領バージン諸島';
$string['country.vi'] = '米国領バージン諸島';
$string['country.wf'] = 'ワリス・フテュナ諸島';
$string['country.eh'] = '西サハラ';
$string['country.ye'] = 'イエメン共和国';
$string['country.zm'] = 'ザンビア共和国';
$string['country.zw'] = 'ジンバブエ共和国';
$string['nocountryselected'] = '国が選択されていません。';
$string['system'] = 'システム';
$string['done'] = '完了';
$string['back'] = '戻る';
$string['backto'] = '%s に戻る';
$string['alphabet'] = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z';
$string['Created'] = '作成';
$string['Updated'] = '更新';
$string['Total'] = '合計';
$string['Visits'] = '訪問';
$string['after'] = '後';
$string['before'] = '前';
$string['Visibility'] = '可視性';
$string['nusers'] = '%s ユーザ';
$string['hidden'] = '非表示';
$string['lastupdate'] = '最終更新日時';
$string['lastupdateorcomment'] = '最終更新日時またはコメント';
$string['Title'] = 'タイトル';
$string['anonymoususer'] = '(作成者名非表示)';
$string['importedfrom'] = 'インポート元 %s';
$string['incomingfolderdesc'] = '他のネットワークホストからインポートされたファイル';
$string['remotehost'] = 'リモートホスト %s';
$string['Copyof'] = '%s のコピー';
$string['loggedinusersonly'] = 'ログインユーザのみ';
$string['allowpublicaccess'] = 'パブリックアクセス (ログインなし) を許可する';
$string['viewmyprofilepage'] = 'プロファイルページを表示する';
$string['editmyprofilepage'] = 'プロファイルページを編集する';
$string['usersprofile'] = '%s のプロファイル';
$string['profiledescription'] = 'プロファイルページはあなたの表示名またはプロファイル写真をクリックしたユーザが閲覧することになるページです。';
$string['mydashboard'] = 'マイダッシュボード';
$string['editdashboard'] = 'ダッシュボードを編集する';
$string['usersdashboard'] = '%s のダッシュボード';
$string['dashboarddescription'] = 'ダッシュボードページはあなたがログインして最初にホームページ上で閲覧することになるページです。このページはあなたのみアクセスすることができます。';
$string['topicsimfollowing'] = '私がフォローしているトピック';
$string['inboxblocktitle'] = '受信箱';
$string['mymessages'] = 'マイメッセージ';
$string['pleasedonotreplytothismessage'] = 'このメッセージに返信しないでください。';
$string['deleteduser'] = '削除済みユーザ';
$string['theme'] = 'テーマ';
$string['choosetheme'] = 'テーマを選択する ...';
$string['Hide2'] = '情報ボックスを隠す';
$string['create'] = '作成';
$string['createsubtitle'] = 'あなたのポートフォリオを開発する';
$string['createdetail'] = 'あなたのeポートフォリオを柔軟な個人学習環境で作成します。';
$string['share'] = '共有';
$string['sharesubtitle'] = 'あなたのプライバシーを管理する';
$string['sharedetail'] = 'あなたの成果および発展をあなたが管理できるスペースで共有します。';
$string['engage'] = '参加';
$string['engagesubtitle'] = '人を探してグループに参加する';
$string['engagedetail'] = 'ディスカッションフォーラムで他の人と関係して、グループ内で共同します。';
$string['howtodisable'] = 'あなたは情報ボックスを非表示にしました。あなたはこのボックスの可視性を<a href="%s">設定</a>ページにてコントロールすることができます。';
$string['setblocktitle'] = 'ブロックタイトルを設定する';
$string['filenotfound'] = 'ファイルが見つかりませんでした。';
$string['filenotfoundmaybeexpired'] = 'ファイルが見つかりませんでした。あなたのエクスポートファイルは作成後24時間のみ存在します。あなたのコンテンツを再度エクスポートする必要があります。';
$string['betweenxandy'] = '範囲: %s - %s';
$string['cleanurlallowedcharacters'] = '半角英数小文字および「-」のみ許可されます。';
$string['content'] = 'コンテンツ';
$string['modified'] = '修正';
$string['historical'] = '歴史的データ';
$string['institutions'] = 'インスティテューション';
$string['members'] = 'メンバー';
$string['blocks'] = 'ブロック';
$string['artefacts'] = 'アーティファクト';
$string['posts'] = '投稿';
$string['facebookdescription'] = 'Maharaはeポートフォリオおよびソーシャルネットワーキングを構築するためのオープンソースウェブアプリケーションです。
Maharaは自分の学習に関するポートフォリオを作成および管理するためのツールをユーザに提供します。また、ソーシャルネットワーキング機能により、ユーザ同士が相互に交流を図ることができます。';
$string['wanttoleavewithoutsaving?'] = 'あなたは修正しました - 修正を保存せずにページを離れてもよろしいですか?';
$string['attachedimage'] = '添付イメージ';
$string['imagebrowsertitle'] = 'イメージを挿入または選択する';
$string['imagebrowserdescription'] = '外部イメージURLを貼り付け、または下記のイメージブラウザを使用してあなたのイメージを選択またはアップロードしてください。';
$string['url'] = 'イメージURL';
$string['style'] = 'スタイル (CSS)';
$string['dimensions'] = 'サイズ';
$string['constrain'] = '縦横比を保持する';
$string['vspace'] = '垂直方向の余白';
$string['hspace'] = '水平方向の余白';
$string['border'] = '罫線幅';
$string['alignment'] = '位置揃え';

?>

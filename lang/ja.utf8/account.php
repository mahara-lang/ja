<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-09-15 08:52:45 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['changepassworddesc'] = 'あなたのパスワードを変更したい場合、ここで詳細を入力してください。';
$string['changepasswordotherinterface'] = 'あなたは異なるインターフェースで<a href="%s">パスワードを変更する</a>ことができます。';
$string['oldpasswordincorrect'] = 'これはあなたの現在のパスワードではありません。';
$string['changeusernameheading'] = 'ユーザ名を変更する';
$string['changeusername'] = '新しいユーザ名';
$string['changeusernamedesc'] = 'あなたが %s へのログインに使用するユーザ名です。ユーザ名の長さは半角3-30文字にしてください。ユーザ名には空白を除く、半角英数字および標準的な記号を使用することができます。';
$string['usernameexists'] = 'このユーザ名は取得されています。別のユーザ名を選択してください。';
$string['accountoptionsdesc'] = 'あなたはここで一般アカウントオプションを設定することができます。';
$string['changeprofileurl'] = 'プロファイルURLを変更する';
$string['profileurl'] = 'プロファイルURL';
$string['profileurldescription'] = 'あなたのプロファイルページのURLです。このフィールドの長さは半角3-30文字にしてください。';
$string['urlalreadytaken'] = 'このプロファイルURLはすでに取得されています。別のURLを選択してください。';
$string['friendsnobody'] = 'だれも私をフレンドとして追加できません。';
$string['friendsauth'] = '新しいフレンドになるには私の承認が必要です。';
$string['friendsauto'] = '新しいフレンドは自動的に承認されます。';
$string['friendsdescr'] = 'フレンドコントロール';
$string['updatedfriendcontrolsetting'] = 'フレンドコントロールが更新されました。';
$string['wysiwygdescr'] = 'HTMLエディタ';
$string['on'] = 'On';
$string['off'] = 'Off';
$string['disabled'] = '無効';
$string['enabled'] = '有効';
$string['licensedefault'] = 'デフォルトライセンス';
$string['licensedefaultdescription'] = 'あなたのコンテンツのデフォルトライセンスです。';
$string['licensedefaultinherit'] = 'インスティテューションデフォルトを使用する';
$string['messagesdescr'] = '他のユーザからのメッセージ';
$string['messagesnobody'] = 'だれも私にメッセージを送信できません。';
$string['messagesfriends'] = 'マイフレンドリストに登録したユーザは私にメッセージを送信できます。';
$string['messagesallow'] = 'だれでも私にメッセージを送信できます。';
$string['language'] = '言語';
$string['showviewcolumns'] = 'ページの編集時、カラムの「追加」および「削除」ボタンを表示する';
$string['tagssideblockmaxtags'] = 'クラウド内の最大タグ数';
$string['tagssideblockmaxtagsdescription'] = 'あなたのタグクラウドに表示されるタグの最大数です。';
$string['enablemultipleblogs1'] = '複数日誌';
$string['enablemultipleblogsdescription'] = 'デフォルトでは、あなたは1つの日誌のみMaharaに作成することができます。あなたが1つ以上の日誌を作成したい場合、このオプションをチェックしてください。';
$string['hiderealname'] = '実名を隠す';
$string['hiderealnamedescription'] = 'あなたがニックネームを設定した場合、そして、あなたの実名検索により、他のユーザから自分を探されたくない場合、このチェックボックスをチェックしてください。';
$string['showhomeinfo2'] = 'ダッシュボード情報';
$string['showhomeinfodescription1'] = 'ダッシュボード上に %s の使用方法に関する情報を表示します。';
$string['showprogressbar'] = 'プロファイル完了プログレスバー';
$string['showprogressbardescription'] = 'プログレスバーおよびあなたの %s プロファイルを完了するためのヒントを表示します。';
$string['mobileuploadtoken'] = 'モバイルアップロードトークン';
$string['badmobileuploadtoken'] = '申し訳ございません、アップロードトークンが無効です - トークンの長さは少なくとも半角6文字にしてください。';
$string['prefssaved'] = 'プリファレンスが保存されました。';
$string['prefsnotsaved'] = 'あなたのプリファレンスの保存に失敗しました!';
$string['maildisabled'] = 'Eメールが無効にされました。';
$string['disableemail'] = 'Eメールを無効にする';
$string['maildisabledbounce'] = '多くのメッセージがサーバに戻ってきたため、あなたのメールアドレス宛のメール送信は停止されました。アカウントプリファレンスの %s にて、Eメールを有効にする前に、あなたのEメールアカウントが正常に動作しているか確認してください。';
$string['maildisableddescription'] = 'あなたのアカウント宛のメール送信は停止されました。アカウントプリファレンスページにて、<a href="%s">あなたのEメールを再度有効</a>にすることができます。';
$string['deleteaccount'] = 'アカウントを削除する';
$string['deleteaccountdescription'] = 'アカウントを削除した場合、今後、あなたのプロファイル情報およびページを他のユーザが閲覧できないようになります。あなたが投稿したフォーラム投稿のコンテンツは閲覧することができますが、投稿者名が表示されないようになります。';
$string['accountdeleted'] = 'あなたのアカウントが正常に削除されました。';
$string['resizeonuploaduserdefault1'] = 'アップロード時、大きなイメージをリサイズする';
$string['resizeonuploaduserdefaultdescription1'] = 'チェックした場合、「アップロード時、大きなイメージをリサイズする」オプションがデフォルトで有効にされます。また、最大高さおよび幅を超えたイメージはアップロード時にリサイズされます。個別のイメージアップロード時、あなたはこのデフォルト設定を無効にすることができます。';
$string['devicedetection'] = 'デバイス検出';
$string['devicedetectiondescription'] = 'このサイトの閲覧時のモバイルデバイス検出を有効にします。';

?>

<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-10-01 05:01:34 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'プロファイル情報';
$string['description'] = '表示するプロファイル情報を選択してください。';
$string['aboutme'] = '私について';
$string['fieldstoshow'] = '表示するフィールド';
$string['introtext'] = '自己紹介テキスト';
$string['useintroductioninstead'] = '「表示するフィールド」の「自己紹介」チェックボックスをチェックして、このフィールドを空白にすることで、あなたのプロファイルの「自己紹介」フィールドを代わりに使用することができます。';
$string['dontshowprofileicon'] = 'プロファイル写真を表示しません。';
$string['dontshowemail'] = 'メールアドレスを表示しません。';
$string['uploadaprofileicon'] = 'あなたにはプロファイル写真がありません。<a href="%sartefact/file/profileicons.php" target="_blank">アップロードしてください</a>。';
$string['dontshowsocialprofiles'] = 'ソーシャルプロファイルを表示しない';
$string['showsocialprofiles'] = '選択したソーシャルプロファイルを表示する';

?>

<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-11-19 05:16:24 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'コメント';
$string['Comment'] = 'コメント';
$string['Comments'] = 'コメント';
$string['comment'] = 'コメント';
$string['comments'] = 'コメント';
$string['addcomment'] = 'コメントを追加する';
$string['Allow'] = '許可';
$string['allowcomments'] = 'コメントを許可する';
$string['approvalrequired'] = 'コメントはモデレートされています。あなたがこのコメントをパブリックにした場合、オーナーにより承認されるまで、他のユーザに表示されることはありません。';
$string['attachfile'] = '添付ファイル';
$string['Attachments'] = '添付ファイル';
$string['cantedithasreplies'] = 'あなたは最新のコメントのみ編集することができます。';
$string['canteditnotauthor'] = 'あなたはこのコメントの投稿者ではありません。';
$string['cantedittooold'] = 'あなたは投稿後 %d 分以内のコメントのみ編集することができます。';
$string['commentmadepublic'] = 'コメントがパブリックにされました。';
$string['commentdeletedauthornotification'] = 'あなたの %s のコメントが削除されました:';
$string['commentdeletednotificationsubject'] = '%s のコメントが削除されました。';
$string['commentnotinview'] = 'コメント %d はページ %d にありません。';
$string['commentratings'] = 'コメントレーティングを有効にする';
$string['commentremoved'] = 'コメントが削除されました。';
$string['commentremovedbyauthor'] = '投稿者によりコメントが削除されました。';
$string['commentremovedbyowner'] = 'オーナーによりコメントが削除されました。';
$string['commentremovedbyadmin'] = '管理者によりコメントが削除されました。';
$string['commentupdated'] = 'コメントが更新されました。';
$string['editcomment'] = 'コメントを編集する';
$string['editcommentdescription'] = 'あなたは新しい返信が追加されていない %d 分以内のコメントを更新することができます。この時間終了後、あなたはコメントを削除して、新たなコメントを追加することはできます。';
$string['entriesimportedfromleapexport'] = 'LEAPエクスポートよりインポートされた、他の場所にインポートできなかったエントリです。';
$string['feedback'] = 'フィードバック';
$string['feedbackattachdirname'] = 'コメントファイル';
$string['feedbackattachdirdesc'] = 'あなたのポートフォリオのコメントに添付されたファイル';
$string['feedbackattachmessage'] = 'あなたの %s フォルダに添付ファイルが追加されました。';
$string['feedbackonviewbyuser'] = '%s のフィードバック by %s';
$string['feedbacksubmitted'] = 'フィードバックが送信されました。';
$string['feedbacksubmittedmoderatedanon'] = 'フィードバックが送信されました。モデレーションを待っています。';
$string['feedbacksubmittedprivateanon'] = 'プライベートフィードバックが送信されました。';
$string['lastcomment'] = '最新のコメント';
$string['makepublic'] = 'パブリックにする';
$string['makepublicnotallowed'] = 'あなたはこのコメントをパブリックにすることはできません。';
$string['makepublicrequestsubject'] = 'リクエスト: プライベートコメントをパブリックに変更する';
$string['makepublicrequestbyauthormessage'] = 'あなたがコメントをパブリックにするよう、%s がリクエストしました。';
$string['makepublicrequestbyownermessage'] = 'あなたがあなたのコメントをパブリックにするよう、%s がリクエストしました。';
$string['makepublicrequestsent'] = 'コメントをパブリックにするリクエストメッセージが %s に送信されました。';
$string['messageempty'] = 'あなたのメッセージは空白です。メッセージまたは添付ファイルを入力してください。';
$string['Moderate'] = 'モデレート';
$string['moderatecomments'] = 'コメントをモデレートする';
$string['moderatecommentsdescription'] = 'あなたに承認されるまで、コメントはプライベートのままにされます。';
$string['newfeedbacknotificationsubject'] = '%s の新しいフィードバック';
$string['placefeedback'] = 'フィードバックを投稿する';
$string['progress_feedback'] = '%s ユーザページのコメント';
$string['rating'] = 'レーティング';
$string['reallydeletethiscomment'] = '本当にこのコメントを削除してもよろしいですか?';
$string['thiscommentisprivate'] = 'このコメントはプライベート (非公開) です。';
$string['typefeedback'] = 'フィードバック';
$string['viewcomment'] = 'コメントを表示する';
$string['youhaverequestedpublic'] = 'あなたはこのコメントをパブリックにするよう、リクエストしました。';
$string['feedbacknotificationhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>%s が %s にコメントを投稿しました。</strong><br />%s</div>

<div style="margin: 1em 0;">%s</div>

<div style="font-size: smaller; border-top: 1px solid #999;">
<p><a href="%s">このコメントにオンライン上で返信する</a></p>
</div>';
$string['feedbacknotificationtext'] = '%s が %s にコメントを投稿しました。
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
オンライン上でコメントを閲覧および返信するには、次のリンクをクリックしてください:
%s';
$string['feedbackdeletedhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>%s のコメントが削除されました。</strong><br />%s</div>

<div style="margin: 1em 0;">%s</div>

<div style="font-size: smaller; border-top: 1px solid #999;">
<p><a href="%s">%s</a></p>
</div>';
$string['feedbackdeletedtext'] = '%s のコメントが削除されました。
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
%s をオンライン上で閲覧するには、次のリンクをクリックしてください:
%s';
$string['artefactdefaultpermissions'] = 'デフォルトのコメントパーミッション';
$string['artefactdefaultpermissionsdescription'] = '選択されたアーティファクトでは、作成時にコメントの投稿が有効にされます。ユーザはそれぞれのアーティファクトにて、これらの設定をオーバーライドすることができます。';

?>

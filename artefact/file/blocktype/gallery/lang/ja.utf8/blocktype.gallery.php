<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-12-04 04:53:26 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'イメージギャラリー';
$string['description1'] = 'あなたのファイルエリアまたは外部ギャラリーからのイメージコレクションです。';
$string['select'] = 'イメージ選択';
$string['selectfolder'] = '私のフォルダの1つからイメージすべてを表示します (後でアップロードされたイメージを含む)。';
$string['selectimages'] = '表示するイメージを私が選択します。';
$string['selectexternal'] = '外部ギャラリーよりイメージを表示します。';
$string['externalgalleryurl'] = 'ギャラリーURLまたはRSS';
$string['externalgalleryurldesc'] = 'あなたは次の外部ギャラリーを埋め込むことができます:';
$string['width'] = '幅';
$string['widthdescription'] = 'あなたのイメージの幅 (ピクセル) を指定してください。イメージがこの幅にサイズ変更されます。イメージのオリジナルサイズを使用する場合、空白のままにしてください。';
$string['style'] = 'スタイル';
$string['stylethumbs'] = 'サムネイル';
$string['stylesquares'] = 'サムネイル (正方形)';
$string['styleslideshow'] = 'スライドショー';
$string['showdescriptions'] = '説明を表示する';
$string['showdescriptionsdescription'] = 'それぞれのイメージに説明を表示する場合、このオプションを有効にしてください。';
$string['cannotdisplayslideshow'] = 'スライドショーを表示できません。';
$string['gallerysettings'] = 'ギャラリー設定';
$string['useslimbox2'] = 'Slimbox 2を使用する';
$string['useslimbox2desc'] = 'Slimbox 2 (Lightbox 2のビジュアルクローン) は現在のページにイメージをオーバーレイするためのシンプルで控えめなスクリプトです。';
$string['photoframe'] = 'フォトフレームを使用する';
$string['photoframedesc'] = 'チェックした場合、ギャラリー内のそれぞれの写真のサムネイルの周りにフレームが描画されます。';
$string['previewwidth'] = '最大写真幅';
$string['previewwidthdesc'] = 'Slimbox 2で表示される場合、写真がリサイズされる最大幅を設定してください。';
$string['flickrsettings'] = 'Flickr設定';
$string['flickrapikey'] = 'Flickr APIキー';
$string['flickrapikeydesc'] = 'Flickrのフォトセットを表示するには、あなたは有効なFlickr APIキーが必要です。<a href="https://www.flickr.com/services/api/keys/apply/" target="_blank">あなたのキーをオンラインで取得してください</a>。';
$string['flickrsets'] = 'Flickrセット';
$string['pbsettings'] = 'Photobucket設定';
$string['pbapikey'] = 'Photobucket APIキー';
$string['pbapikeydesc'] = 'Photobucketのフォトアルバムを表示するには、あなたは有効なAPIキーおよびAPIプライベートキーが必要です。<br /><a href="http://developer.photobucket.com/" target="_blank">Photobucket開発者ウェブサイト</a>にアクセスして利用規約に同意、サインアップした後、APIキーを取得してください。';
$string['pbapiprivatekey'] = 'Photobucket APIプライベートキー';
$string['photobucketphotosandalbums'] = 'Photobucketユーザ写真およびアルバム';
$string['Photo'] = '写真';
$string['by'] = 'by';
$string['panoramiocopyright'] = 'Panoramioから提供された写真は、そのオーナーが著作権を有します。';
$string['panoramiouserphotos'] = 'Panoramioユーザ写真';
$string['picasaalbums'] = 'Picasaアルバム';
$string['windowslivephotoalbums'] = 'Windows Liveフォトギャラリーアルバム';
$string['externalnotsupported'] = 'あなたが提供した外部URLはサポートされていません。';

?>

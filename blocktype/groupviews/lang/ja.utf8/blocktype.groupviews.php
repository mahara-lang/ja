<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2015-02-05 16:43:56 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'グループページ';
$string['description'] = 'グループ内で共有されるページを表示します。';
$string['displaygroupviews'] = 'グループページを表示する';
$string['displaygroupviewsdesc'] = 'グループページ - グループ内で作成されたページ';
$string['displaysharedviews'] = '共有ページを表示する';
$string['displaysharedviewsdesc1'] = 'このグループと共有されているページ (コレクション内のページを除く) の一覧を表示します。';
$string['displaysharedcollections'] = '共有コレクションを表示する';
$string['displaysharedcollectionsdesc'] = 'このグループと共有されているコレクションの一覧を表示します。';
$string['displaysubmissions'] = '送信ページおよびコレクションを表示する';
$string['displaysubmissionsdesc'] = 'このグループに送信されたページおよびコレクションの一覧を表示します。';
$string['defaulttitledescription'] = 'タイトルフィールドを空白にした場合、デフォルトタイトルが適用されます。';
$string['itemstoshow'] = '1ページあたりのエントリ数';
$string['itemstoshowdesc'] = 'それぞれのセクションで表示されるページまたはコレクション数です。最大: 100';
$string['showbyanybody'] = '誰でも';
$string['showbygroupmembers'] = 'このグループのメンバー';
$string['shownone'] = 'なし';
$string['sortgroupviewstitle'] = 'グループページを並べ替える';
$string['sortsharedviewstitle'] = '共有ページおよびコレクションを並べ替える';
$string['sortsubmittedtitle'] = '送信済みページおよびコレクションを並べ替える';
$string['sortviewsbyalphabetical'] = 'アルファベット順';
$string['sortviewsbylastupdate'] = '最も最近の更新順';
$string['sortviewsbytimesubmitted'] = '最も最近の送信順';

?>

<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2012-09-07 03:41:00 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'ウォッチページ';
$string['description'] = 'あなたのウォッチリストのページを表示します。';
$string['nopages'] = 'あなたのウォッチリストにページはありません。';
$string['itemstoshow'] = '表示するアイテム数';

?>

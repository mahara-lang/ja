<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-05-27 20:13:28 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['element.bytes.bytes'] = 'B';
$string['element.bytes.kilobytes'] = 'KB';
$string['element.bytes.megabytes'] = 'MB';
$string['element.bytes.gigabytes'] = 'GB';
$string['element.bytes.invalidvalue'] = '値は数値にしてください';
$string['element.calendar.invalidvalue'] = '無効な日付/時間が指定されました';
$string['element.calendar.opendatepicker'] = '日付ピッカを開く';
$string['element.date.monthnames'] = '1月,2月,3月,4月,5月,6月,7月,8月,9月,10月,11月,12月';
$string['element.date.specify'] = '日付設定';
$string['element.date.at'] = '-';
$string['element.expiry.days'] = '日';
$string['element.expiry.weeks'] = '週';
$string['element.expiry.months'] = '月';
$string['element.expiry.years'] = '年';
$string['element.expiry.noenddate'] = '終了日なし';
$string['element.files.addattachment'] = '添付を追加する';
$string['element.select.other'] = 'その他';
$string['element.color.transparent'] = '透明';
$string['rule.before.before'] = 'フィールド「 %s 」の後に設定することはできません';
$string['rule.email.email'] = 'メールアドレスが有効ではありません';
$string['rule.integer.integer'] = 'フィールドは整数にしてください';
$string['rule.maxlength.maxlength'] = 'このフィールドは最大半角 %d 文字にしてください';
$string['rule.minlength.minlength'] = 'このフィールドは少なくとも半角 %d 文字にしてください';
$string['rule.minvalue.minvalue'] = 'この値は %d 以下にすることはできません';
$string['rule.regex.regex'] = 'このフィールドは正しい形式ではありません';
$string['rule.required.required'] = 'このフィールドは必須入力フィールドです。';
$string['rule.validateoptions.validateoptions'] = 'オプション「 %s 」が正しくありません';
$string['rule.maxvalue.maxvalue'] = 'この値は %d 以下にしてください';

?>

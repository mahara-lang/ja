<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2013-12-23 09:52:17 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'フォルダ';
$string['description'] = 'あなたのファイルエリアの単独フォルダです (「コンテンツ -> ファイル」をご覧ください)。';
$string['defaulttitledescription'] = 'ここを空白にした場合、フォルダのタイトルが使用されます。';
$string['foldersettings'] = 'フォルダ設定';
$string['defaultsortorder'] = 'ファイルのデフォルト並び順';

?>

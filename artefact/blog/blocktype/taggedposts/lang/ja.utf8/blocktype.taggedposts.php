<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2015-01-29 01:28:54 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'タグ付き日誌エントリ';
$string['description'] = '特定のタグが付けられた日誌エントリを表示します (「コンテンツ -> 日誌」をご覧ください)。';
$string['blockheadingtags'] = 'タグ「 %2$s 」の日誌エントリ';
$string['blockheadingtagsomit'] = '(タグ「 %2$s 」を除外)';
$string['defaulttitledescription'] = 'ここを空白にした場合、日誌のタイトルが使用されます。';
$string['postsperpage'] = '1ページあたりのエントリ数';
$string['taglist'] = 'マイタグ';
$string['taglistdesc'] = 'あなたが除外したいタグの前にマイナス記号を入力してください。これらのタグは青色の背景色で表示されます。';
$string['excludetag'] = '除外タグ:';
$string['notags'] = '「 %s 」がタグ付けされた記事はありません。';
$string['notagsboth'] = '「 %s 」 (「 %s 」を除外) がタグ付けされた記事はありません。';
$string['notagsavailable'] = 'あなたはタグを作成していません。';
$string['notagsavailableerror'] = 'タグが選択されていません - ここで選択する前に、あなたの日誌エントリにタグを追加する必要があります。';
$string['postedin'] = '-';
$string['postedon'] = '-';
$string['itemstoshow'] = '表示するアイテム数';
$string['configerror'] = 'ブロック設定中にエラーが発生しました。';
$string['showjournalitemsinfull'] = '日誌アイテムをフルに表示する';
$string['showjournalitemsinfulldesc'] = 'チェックした場合、日誌エントリが表示されます。そうでない場合、日誌エントリのタイトルのみ表示されます。';
$string['tag'] = 'タグ';

?>

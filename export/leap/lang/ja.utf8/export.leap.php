<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2013-01-18 05:39:08 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Leap2A';
$string['description'] = '<a href="http://wiki.leapspecs.org/2A/specification">Leap2Aスタンダードフォーマット</a>のエクスポートを作成します。あなたは後で、このデータを<a href="http://wiki.mahara.org/Developer_Area/Import//Export/Interoperability">他のLeap2A互換システム</a>にインポートすることができます。エクスポートは人間に読むことが難しいフォーマットで作成されます。';

?>

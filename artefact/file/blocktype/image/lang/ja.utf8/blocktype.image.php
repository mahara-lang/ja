<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2012-03-02 15:14:59 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'イメージ';
$string['description'] = 'あなたのファイルエリアの単独イメージです。';
$string['showdescription'] = '説明を表示しますか?';
$string['width'] = '幅';
$string['widthdescription1'] = 'あなたのイメージの幅 (ピクセル) を指定してください。イメージがこの幅にサイズ変更されます。イメージのオリジナルサイズを使用する場合、空白のままにしてください。オリジナルサイズが大きすぎる場合、ブロック幅に縮小されます。';

?>

<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2012-09-17 04:58:07 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'スタンドアロンHTMLウェブサイト';
$string['description'] = 'あなたのポートフォリオデータを内蔵したウェブサイトを作成します。あなたはこのファイルをインポートすることはできませんが、標準的なウェブブラウザで閲覧することができます。';
$string['usersportfolio'] = '%s - ポートフォリオ';
$string['preparing'] = '%s を準備中';
$string['exportingdatafor'] = '%s のデータをエクスポート中';
$string['buildingindexpage'] = 'インデックスページを構築中';
$string['copyingextrafiles'] = '追加ファイルをコピー中';
$string['duplicatepagetitle'] = 'ページタイトル重複のため、エクスポートに失敗しました。タイトルがユニークであることを確認してください。';

?>

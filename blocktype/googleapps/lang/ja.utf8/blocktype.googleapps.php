<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-08-18 00:20:14 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Google Apps';
$string['description'] = 'Googleカレンダーおよびドキュメントを埋め込みます。';
$string['appscodeorurl'] = 'コードまたはURLを埋め込む';
$string['appscodeorurldesc'] = 'Google Appsを外部から閲覧できるページのコードまたはURLを貼り付けてください。';
$string['height'] = '高さ';
$string['badurlerror'] = '埋め込みコードまたはURLを構文解析できません: %s';

?>

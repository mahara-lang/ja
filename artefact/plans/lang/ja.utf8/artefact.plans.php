<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-04-13 16:51:50 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'プラン';
$string['canteditdontownplan'] = 'あなたの所有ではないため、このプランを編集することはできません。';
$string['description'] = '説明';
$string['deleteplanconfirm'] = '本当にこのプランを削除してもよろしいですか? このプランを削除することで、プランの中に含まれているすべてのタスクも削除されます。';
$string['deleteplan'] = 'プランを削除する';
$string['deletethisplan'] = 'プランを削除する: %s';
$string['editplan'] = 'プランを編集する';
$string['editingplan'] = 'プランの編集';
$string['managetasks'] = 'タスクを管理する';
$string['managetasksspecific'] = '「 %s 」のタスクを管理する';
$string['newplan'] = '新しいプラン';
$string['noplansaddone'] = 'まだプランはありません。%sプランを追加してください%s!';
$string['noplans'] = '表示するプランはありません。';
$string['plan'] = 'プラン';
$string['plans'] = 'プラン';
$string['Plans'] = 'プラン';
$string['plandeletedsuccessfully'] = 'プランが正常に削除されました。';
$string['plannotdeletedsuccessfully'] = 'プランの削除中にエラーが発生しました。';
$string['plannotsavedsuccessfully'] = 'このフォームの送信中にエラーが発生しました。マークされたフィールドを確認して再度お試しください。';
$string['plansavedsuccessfully'] = 'プランが正常に保存されました。';
$string['planstasks'] = 'プラン「 %s 」のタスク';
$string['planstasksdescription'] = 'あなたのプランを作成するには、以下にタスクを追加するか、「 %s 」ボタンを使用してください。';
$string['saveplan'] = 'プランを保存する';
$string['title'] = 'タイトル';
$string['titledesc'] = 'それぞれの「マイプラン」ブロックタイプ内タスクにタイトルを表示するために使用されます。';
$string['alltasks'] = 'すべてのタスク';
$string['canteditdontowntask'] = 'あなたの所有ではないため、このタスクを編集することはできません。';
$string['completed'] = '完了';
$string['incomplete'] = '未完了';
$string['overdue'] = '期限超過';
$string['completiondate'] = '完了予定日';
$string['completeddesc'] = 'あなたのプランを完了済みにします。';
$string['deletetaskconfirm'] = '本当にこのタスクを削除してもよろしいですか?';
$string['deletetask'] = 'タスクを削除する';
$string['deletethistask'] = 'タスクを削除する: %s';
$string['edittask'] = 'タスクを編集する';
$string['editingtask'] = 'タスクの編集';
$string['mytasks'] = 'マイタスク';
$string['newtask'] = '新しいタスク';
$string['notasks'] = '表示するタスクはありません。';
$string['notasksaddone'] = 'まだタスクはありません。%sタスクを追加してください%s!';
$string['savetask'] = 'タスクを保存する';
$string['task'] = 'タスク';
$string['Task'] = 'タスク';
$string['tasks'] = 'タスク';
$string['Tasks'] = 'タスク';
$string['taskdeletedsuccessfully'] = 'タスクが正常に削除されました。';
$string['tasksavedsuccessfully'] = 'タスクが正常に保存されました。';
$string['ntasks'] = '%s タスク';
$string['duplicatedplan'] = '複製プラン';
$string['existingplans'] = '既存プラン';
$string['duplicatedtask'] = '複製タスク';
$string['existingtasks'] = '既存タスク';
$string['progress_plan'] = '%s プランを追加する';
$string['progress_task'] = '%s タスクをプランに追加する';

?>

<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2012-09-07 03:33:06 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['browserid'] = 'Persona';
$string['title'] = 'Persona';
$string['description'] = 'Personaで認証する';
$string['notusable'] = 'PHP cURL拡張モジュールをインストールした後、Personaベリファイアへの接続をチェックしてください。';
$string['badassertion'] = 'Personaアサーション (表明) が有効ではありません: %s';
$string['badverification'] = 'Personaベリファイアの有効なJSONアウトプットをMaharaが受信しませんでした。';
$string['login'] = 'Persona';
$string['register'] = 'Persona登録';
$string['missingassertion'] = 'Personaが英数字アサーション (表明) を戻しませんでした。';
$string['emailalreadyclaimed'] = 'すでに他のユーザアカウントがメールアドレス「 %s 」を取得しています。';
$string['emailclaimedasusername'] = 'すでに他のユーザアカウントがメールアドレス「 %s 」をユーザ名として取得しています。';
$string['browseridnotenabled'] = 'すべてのアクティブなインスティテューションにおいて、Persona認証プラグインは有効にされていません。';
$string['emailnotfound'] = 'Personaが有効にされているインスティテューションすべてにおいて、メールアドレス「 %s 」のユーザは見つかりませんでした。';

?>

<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2013-08-21 04:13:01 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = '日誌';
$string['description'] = '日誌全体です (「コンテンツ -> 日誌」をご覧ください)。';
$string['defaulttitledescription'] = 'ここを空白にした場合、日誌のタイトルが使用されます。';
$string['postsperpage'] = '1ページあたりの記事数';

?>

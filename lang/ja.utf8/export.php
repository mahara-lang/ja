<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2015-02-12 13:46:28 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['allmydata'] = '私のすべてのデータ';
$string['chooseanexportformat'] = 'エクスポートフォーマットを選択する';
$string['clicktopreview'] = 'プレビューするには、ここをクリック';
$string['collectionstoexport'] = 'エクスポートするコレクション';
$string['creatingzipfile'] = 'ZIPファイル作成中';
$string['Done'] = '完了';
$string['Export'] = 'エクスポート';
$string['clickheretodownload'] = 'ダウンロードするには、ここをクリック';
$string['continue'] = '続ける';
$string['exportgeneratedsuccessfully'] = 'エクスポートが正常に生成されました。%sダウンロードするには、ここをクリック%s';
$string['exportgeneratedsuccessfully1'] = 'エクスポートが正常に生成されました。';
$string['exportgeneratedwitherrors'] = 'エクスポートが生成されましたが、いくつかのエラーが発生しています。';
$string['exportingartefactplugindata'] = 'アーティファクトプラグインデータのエクスポート中';
$string['exportingartefacts'] = 'アーティファクトのエクスポート中';
$string['exportingartefactsprogress'] = 'アーティファクトのエクスポート中: %s/%s';
$string['exportingfooter'] = 'フッタのエクスポート中';
$string['exportingviews'] = 'ページのエクスポート中';
$string['exportingcollections'] = 'コレクションのエクスポート中';
$string['exportingviewsprogress'] = 'ページのエクスポート中: %s/%s';
$string['exportportfoliodescription'] = 'このツールでは、あなたのポートフォリオ情報およびページすべてをエクスポートします。あなたのサイト設定およびあなたがグループにアップロードまたは作成したコンテンツはエクスポートされません。';
$string['exportyourportfolio'] = 'あなたのポートフォリオを生成する';
$string['generateexport'] = 'エクスポートを生成する';
$string['noexportpluginsenabled'] = '管理者によりエクスポートプラグインが有効にされていないため、あなたはこの機能を使用することはできません。';
$string['justsomecollections'] = 'いくつかのマイコレクションのみ';
$string['justsomeviews'] = 'いくつかのマイページのみ';
$string['includefeedback'] = 'フィードバックを含む';
$string['includefeedbackdescription'] = 'すべてのユーザコメントがHTMLエクスポートに含まれます。';
$string['nonexistentfile'] = '存在しないファイルの追加を試みました: %s';
$string['nonexistentprofileicon'] = '存在しないプロファイルアイコン「 %s 」を追加しようとしました。';
$string['nonexistentresizedprofileicon'] = '存在しないリサイズ済みプロファイルアイコン「 %s 」を追加しようとしました。';
$string['unabletocopyartefact'] = 'アーティファクトファイル「 %s 」をコピーできませんでした。';
$string['unabletocopyprofileicon'] = 'プロファイルアイコン「 %s 」をコピーできませんでした。';
$string['unabletocopyresizedprofileicon'] = 'リサイズ済みプロファイルアイコン「 %s 」をコピーできませんでした。';
$string['couldnotcreatedirectory'] = 'ディレクトリ「 %s 」を作成できませんでした。';
$string['couldnotcreatestaticdirectory'] = '静的ディレクトリ「 %s 」を作成できませんでした。';
$string['couldnotcopystaticfile'] = '静的ファイル「 %s 」をコピーできませんでした。';
$string['couldnotcopyattachment'] = '添付「 %s 」をコピーできませんでした。';
$string['couldnotcopyfilesfromto'] = 'ディレクトリ「 %s 」から「 %s 」にファイルをコピーできませんでした。';
$string['couldnotwriteLEAPdata'] = 'ファイルにLeap2Aデータを書き込めませんでした。';
$string['pleasewaitwhileyourexportisbeinggenerated'] = 'あなたのエクスポートを生成しています。お待ちください ...';
$string['reverseselection'] = 'すべての選択を解除する';
$string['selectall'] = 'すべてを選択する';
$string['setupcomplete'] = 'セットアップ完了';
$string['Starting'] = '開始';
$string['unabletoexportportfoliousingoptions'] = '選択されたオプションでポートフォリオをエクスポートできません。';
$string['unabletogenerateexport'] = 'エクスポートを生成できません。';
$string['viewstoexport'] = 'エクスポートするページ';
$string['whatdoyouwanttoexport'] = '何をエクスポートしますか?';
$string['writingfiles'] = 'ファイル書き出し中';
$string['youarehere'] = 'あなたはここにいます';
$string['youmustselectatleastonecollectiontoexport'] = 'あなたはエクスポートするコレクションを少なくとも1つ選択する必要があります。';
$string['youmustselectatleastoneviewtoexport'] = 'あなたはエクスポートするページを少なくとも1つ選択する必要があります。';
$string['zipnotinstalled'] = 'あなたのシステムにはZIPコマンドがありません。この機能を有効にするには、ZIPをインストールしてください。';
$string['addedleap2atoexportqueuecollections'] = 'あなたのコレクションをエクスポートキューに追加しました。';
$string['addedleap2atoexportqueueviews'] = 'あなたのページをエクスポートキューに追加しました。';
$string['addedleap2atoexportqueueall'] = 'あなたのデータすべてをエクスポートキューに追加しました。';
$string['exportqueuenotempty'] = 'このユーザのアイテムがエクスポートキューにあります。アーカイブされるまでお待ちください。';
$string['requeue'] = '再度キューに追加する';
$string['unabletoexportportfoliousingoptionsadmin'] = 'アイテムはページまたはコレクションオブジェクトではありません。';
$string['exportzipfileerror'] = 'ZIPファイルの生成に失敗しました: %s';
$string['submissiondirnotwritable'] = '提出アーカイブディレクトリに書き込めません: %s';
$string['exportarchivesavefailed'] = 'データベースにエクスポートアーカイブ情報を保存できません。';
$string['archivedsubmissionfailed'] = 'データベースにアーカイブ済み提出情報を保存できません。';
$string['submissionreleasefailed'] = 'アーカイブ後の提出リリースに失敗しました。';
$string['deleteexportqueueitems'] = 'エクスポートキューアイテムデータベーステーブルのアイテム削除に失敗しました。';
$string['deleteexportqueuerow'] = 'エクスポートキューデータベーステーブルのアイテム削除に失敗しました。';
$string['exportqueueerrorsadminsubject'] = 'キューのエクスポート実行中にエラーが発生しました。';
$string['exportqueueerrorsadminmessage'] = '次のエラーのため行「 %s 」をエクスポートできません: %s';

?>

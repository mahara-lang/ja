<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2009-11-19 05:23:21 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = '埋め込みメディア';
$string['description'] = '埋め込みメディアのファイルを選択してください。';
$string['media'] = 'メディア';
$string['flashanimation'] = 'Flashアニメーション';
$string['typeremoved'] = 'このブロックでは管理者によって許可されないメディアタイプを示します。';
$string['configdesc'] = 'このブロックにユーザが埋め込むことのできるファイルタイプを設定してください。すでにブロックで使用されているファイルタイプを無効にした場合、そのファイルは表示されなくなります。';

?>

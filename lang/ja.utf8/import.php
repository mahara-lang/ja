<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2013-10-28 04:58:01 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['Import'] = 'インポート';
$string['importexceedquota'] = 'あなたがアップロードしたLeap2Aファイルの新しいファイル合計サイズがあなたのクオータを超えるようです。いくつかのファイルを削除して、あなたは再度試みる必要があります。';
$string['importsuccessfully'] = 'あなたのポートフォリオは正常にインポートされました。';
$string['importfailed'] = '<p><strong>申し訳ございません - あなたのLeap2Aファイルのインポートに失敗しました。</strong></p>

<p>あなたが有効なLeap2Aファイルをアップロードしていない、またはこのバージョンのMaharaではあなたのLeap2Aファイルのバージョンをサポートされていないことが考えられます。仮にLeap2Aが有効であったとしても、インポートを失敗させるバグがMaharaに含まれている可能性もあります。</p>

<p>戻って、再度お試しください。問題が持続する場合、サポートを求めるため、<a href="http://mahara.org/forums/">Maharaフォーラム</a>に投稿しても良いでしょう。あなたのファイルのコピーを準備してください。</p>';
$string['importwitherrors'] = 'あなたのポートフォリオはエラーと共ににインポートされました。';
$string['importartefactplugindata'] = 'アーティファクトプラグインデータのインポート';
$string['importartefacts'] = 'アーティファクトのインポート';
$string['importartefactsprogress'] = 'アーティファクトのインポート: %s/%s';
$string['importfooter'] = 'フッタのインポート';
$string['importviews'] = 'ページのインポート';
$string['importviewsprogress'] = 'ページのインポート: %s/%s';
$string['importportfoliodescription'] = 'Leap2Aファイルがある場合、ここで別のサイトからあなたのポートフォリオにコンテンツをインポートすることができます。サイト設定、またはあなたがグループ内にアップロードまたは作成したコンテンツはインポートすることができません。';
$string['importyourportfolio'] = 'あなたのポートフォリオをインポートする';
$string['howimportyourportfolio'] = 'あなたのポートフォリオアイテムのインポート方法を選択する';
$string['howimportportfoliodescription'] = 'このステップでは、あなたの既存のコンテンツとインポートコンテンツをどのように統合するか決定します。';
$string['noimportpluginsenabled'] = 'サイト管理者はインポートプラグインを有効にしていません。そのため、あなたは使用することができません。';
$string['noleapimportpluginsenabled'] = 'サイト管理者はLeap2Aインポートプラグインを有効にしていません。そのため、あなたは使用することができません。';
$string['entry'] = 'エントリ';
$string['entries'] = 'エントリ';
$string['importentry'] = 'エントリをインポートする';
$string['duplicateditem'] = '重複アイテム';
$string['existingitem'] = '既存のアイテム';
$string['howtoimport'] = 'インポート方法';
$string['ignore'] = '無視する';
$string['replace'] = '置換する';
$string['addnew'] = '新しく作成する';
$string['append'] = '追加する';
$string['importfolder'] = 'フォルダをインポートする %s';
$string['importresult'] = 'インポート結果';
$string['noimportentry'] = 'インポートエントリはありません。';

?>

<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2010-10-01 12:51:09 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['memoryused'] = 'メモリー';
$string['timeused'] = '実行時間';
$string['seconds'] = '秒';
$string['included'] = 'インクルードファイル';
$string['dbqueries'] = 'DBクエリ';
$string['reads'] = 'リード';
$string['writes'] = 'ライト';
$string['cached'] = 'キャッシュ';
$string['ticks'] = 'ティック';
$string['sys'] = 'sys';
$string['user'] = 'ユーザ';
$string['cuser'] = 'cuser';
$string['csys'] = 'csys';
$string['serverload'] = 'サーバロード';

?>

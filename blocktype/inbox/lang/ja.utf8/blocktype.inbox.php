<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2012-09-07 03:40:29 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = '受信箱';
$string['description'] = 'あなたの選択された最近の受信箱メッセージを表示します。';
$string['messagetypes'] = '表示するメッセージタイプ';
$string['maxitems'] = '表示するアイテムの最大数';
$string['maxitemsdescription'] = '設定範囲: 1～100';
$string['More'] = 'さらに';
$string['nomessages'] = 'メッセージなし';
$string['defaulttitledescription'] = 'タイトルフィールドを空白にした場合、デフォルトタイトルが適用されます。';

?>

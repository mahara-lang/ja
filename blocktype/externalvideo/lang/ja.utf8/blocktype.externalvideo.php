<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-11-05 16:30:52 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = '外部ビデオ';
$string['description'] = '外部ビデオを埋め込みます。';
$string['urlorembedcode'] = 'URLまたは埋め込みコード';
$string['videourldescription3'] = 'コンテンツが設置されているページの<strong>埋め込みコード</strong>または<strong>URL</strong>を貼り付けてください。';
$string['validiframesites'] = '&lt;iframe&gt;タグを含む以下のサイトの<strong>埋め込みコード</strong>が許可されます:';
$string['validurlsites'] = '以下のサイトの<strong>URL</strong>が許可されます:';
$string['width'] = '幅';
$string['height'] = '高さ';
$string['widthheightdescription'] = '「幅」および「高さ」フィールドはURLのみに使用されます。上記で「embed」または「iframe」タグを入力している場合、あなたはコード内で幅および高さを更新する必要があります。';
$string['invalidurl'] = '無効なURL';
$string['invalidurlorembed'] = '無効なURLまたは埋め込みコード';
$string['googlevideo'] = 'Googleビデオ';
$string['scivee'] = 'SciVee';
$string['youtube'] = 'YouTube';
$string['teachertube'] = 'TeacherTube';
$string['slideshare'] = 'SlideShare';
$string['prezi'] = 'Prezi';
$string['glogster'] = 'Glogster';
$string['vimeo'] = 'Vimeo';
$string['voki'] = 'Voki';
$string['voicethread'] = 'VoiceThread';
$string['wikieducator'] = 'WikiEducator';

?>

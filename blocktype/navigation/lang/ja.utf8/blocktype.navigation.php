<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2013-08-21 04:18:50 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['defaulttitledescription'] = 'ここを空白にした場合、コレクションのタイトルが使用されます。';
$string['collection'] = 'コレクション';
$string['title'] = 'ナビゲーション';
$string['description'] = 'ページのコレクションをシンプルなナビゲーションとして表示します (「ポートフォリオ -> コレクション」、グループ内の場合はコレクションタブをご覧ください)。';
$string['nocollections1'] = 'コレクションはありません。<a href="%s">コレクションを作成してください</a>。';

?>

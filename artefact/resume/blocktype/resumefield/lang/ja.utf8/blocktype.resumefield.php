<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2012-09-10 18:35:51 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'レジュメフィールド';
$string['description'] = 'レジュメ情報を表示します。';
$string['fieldtoshow'] = '表示するフィールド';
$string['filloutyourresume'] = 'さらにフィールドを追加するには、%sあなたのレジュメに内容を入力してください%s。';
$string['defaulttitledescription'] = 'ここを空白にした場合、フィールド名が使用されます。';

?>

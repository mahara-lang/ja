<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-03-15 21:17:59 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['cookieconsent'] = 'クッキーコンセント';
$string['cookieconsentintro1'] = '欧州議会および理事会の2009/136/EC命令は「クッキー命令」または「クッキー法」として知られています。これは欧州におけるクッキー同意に関する必要条件を定義した法律文書です。これは基本的に前の命令「2002/58/EC」の修正案です。そして、ウェブのデータおよびプライバシー、その他の電子コミュニケーションに対して、広範に関係します。';
$string['cookieconsentintro2'] = '新しい命令は2011年5月25日に発効されました。本文は26ページに渡りますが、クッキーに関する最も重要な段落は20ページにあります。これは前の命令「2002/58/EC」の記事5(3)を修正したものです。';
$string['cookieconsentintro3'] = '「加盟国は情報の保存またはすでに保存されている情報へのアクセスに関して、命令95/46/ECに従って明確および包括的な情報を提供されている購読者またはユーザの端末装置において、特に処理の目的に関する同意を得る必要があります。これは技術的保管または電子証券取引ネットワークに関する通信伝送を実現するための唯一のアクセス手段、購読者またはユーザからサービスの提供を求められている厳密に必要な情報社会サービス提供者を阻むものであってはなりません」';
$string['readfulltext1'] = '命令全文を読む';
$string['directive2009136'] = '命令2009/136/EC';
$string['readdirectiveBG'] = '命令2009/136/ECを読む (ブルガリア語)';
$string['readdirectiveCS'] = '命令2009/136/ECを読む (チェコ語)';
$string['readdirectiveDA'] = '命令2009/136/ECを読む (デンマーク語)';
$string['readdirectiveDE'] = '命令2009/136/ECを読む (ドイツ語)';
$string['readdirectiveEL'] = '命令2009/136/ECを読む (ギリシャ語)';
$string['readdirectiveEN'] = '命令2009/136/ECを読む (英語)';
$string['readdirectiveES'] = '命令2009/136/ECを読む (スペイン語)';
$string['readdirectiveET'] = '命令2009/136/ECを読む (エストニア語)';
$string['readdirectiveFI'] = '命令2009/136/ECを読む (フィンランド語)';
$string['readdirectiveFR'] = '命令2009/136/ECを読む (フランス語)';
$string['readdirectiveHU'] = '命令2009/136/ECを読む (ハンガリー語)';
$string['readdirectiveIT'] = '命令2009/136/ECを読む (イタリア語)';
$string['readdirectiveLT'] = '命令2009/136/ECを読む (リトアニア語)';
$string['readdirectiveLV'] = '命令2009/136/ECを読む (ラトビア語)';
$string['readdirectiveMT'] = '命令2009/136/ECを読む (マルタ語)';
$string['readdirectiveNL'] = '命令2009/136/ECを読む (オランダ語)';
$string['readdirectivePL'] = '命令2009/136/ECを読む (ポーランド語)';
$string['readdirectivePT'] = '命令2009/136/ECを読む (ポルトガル語)';
$string['readdirectiveRO'] = '命令2009/136/ECを読む (ルーマニア語)';
$string['readdirectiveSK'] = '命令2009/136/ECを読む (スロバキア語)';
$string['readdirectiveSL'] = '命令2009/136/ECを読む (スロベニア語)';
$string['readdirectiveSV'] = '命令2009/136/ECを読む (スウェーデン語)';
$string['cookieconsentintro4'] = '要するに、コンピュータ、モバイルフォンまたは他のデバイスから情報を保存または検索する前に、ユーザが同意する必要があることを意味します。この目的はエンドユーザのプライバシーを増すこと、そして国民が知ることなしに組織が情報を取得することを防ぐことにあります。';
$string['cookieconsentintro51'] = '最初に下記の%sSilktideによるクッキーコンセントプラグイン%sを有効にして必要なオプションを設定した後、あなたの変更を保存してください。あなたはクッキーコントロールを有効にするため、修正に関する詳細インストラクション、またはあなたのテーマヘッダファイルまたは「$cfg->additionalhtmlhead」設定の更新方法に関して記載された別ページにリダイレクトされます。';
$string['cookieconsentenable'] = 'クッキーコンセントを有効にしますか?';
$string['cookieconsentenabled'] = 'クッキーコンセントが有効にされ、設定が保存されました。';
$string['cookieconsentdisabled'] = 'クッキーコンセントが無効にされました。';
$string['cookieconsent2'] = '追加サイト修正';
$string['additionalmodifications'] = 'クッキーコントロールを完全に有効にしたい場合、あなたのテーマヘッダファイルまたは<tt>$cfg->additionalhtmlhead</tt>設定をを修正または更新する必要があります。';
$string['instructiontext1'] = '%s クッキーを設定するJavaスクリプトを探します。例には %s を含むことができます。';
$string['instructiontext2'] = 'タイプ属性が「text/javascript」から「text/plain」になるよう、<tt>&lt;script&gt;</tt>タグを修正します。';
$string['instructiontext3'] = '「 %s 」のクラスを<tt>&lt;script&gt;</tt>タグに追加する';
$string['example1social'] = 'FacebookボタンおよびTwitterウィジェット';
$string['example1analytics'] = 'Googleアナリティクス‎および統計カウンタ';
$string['example1advertising'] = 'Googleアドセンスおよび他のターゲット広告プラグイン';
$string['example1necessary'] = 'Googleアドセンスおよび他のターゲット広告プラグイン';
$string['example'] = '例';
$string['examplebefore'] = '変更前:';
$string['exampleafter'] = '変更後 (変更は太字):';
$string['itdidntwork'] = '動作しませんでした。';
$string['itdidntwork1'] = '最初に「<b>%s</b>」を「<b>%s</b>」の代わりに使用してください。これにより、<tt>document.write()</tt>を使用するJavaスクリプトプラグインを調整します。';
$string['itdidntwork2'] = 'これが手助けとならない場合、%sクッキーコンセントコード例ページをチェックするか%s、%sクッキーコンセントLinkedInグループ%sにご相談ください。';
$string['generaloptions'] = '一般オプション';
$string['cookietypes'] = 'クッキータイプ';
$string['cookietypesdesc'] = 'あなたのサイトで使用しているクッキータイプを選択してください。';
$string['cookietypessocial'] = 'ソーシャルメディア';
$string['cookietypesanalytics'] = 'アナリティクス';
$string['cookietypesadvertising'] = '広告';
$string['cookietypesnecessary'] = '厳密に必要';
$string['consentmode'] = 'コンセントモード';
$string['consentmodedesc1'] = 'ブラウザの「行動追跡拒否 (do not track)」設定が有効にされている場合、(下記「機能オプション」でオーバーライドされない限り」) クッキーコンセントは常に明示モードを使用します。';
$string['consentmodedesc2'] = '最新バージョンのInternet Explorerでは「行動追跡拒否 (do not track)」設定がデフォルトで有効にされています。';
$string['consentmodeexplicit'] = '明示 - ビジターが承諾するまでクッキーを設定しません。';
$string['consentmodeimplicit'] = '暗黙 - クッキーを設定して、ビジターによる使用停止を許可します。';
$string['stylingoptions'] = 'スタイリングオプション';
$string['pluginstyle'] = 'スタイル';
$string['pluginstyledesc'] = 'クッキーコンセントの概観を変更します。';
$string['pluginstyledark'] = 'ダーク';
$string['pluginstylelight'] = 'ライト';
$string['bannerposition'] = 'バナーポジション';
$string['bannerpositiondesc'] = '画面最上部または最下部のどちらにコンセントバナーを表示するか選択してください。';
$string['bannerpositiontop'] = '最上部';
$string['bannerpositionpush'] = '最上部からプッシュする (実験用)';
$string['bannerpositionbottom'] = '最下部';
$string['tabposition'] = 'タブポジション';
$string['tabpositiondesc'] = 'プライバシー設定タブが表示される場所を選択してください。';
$string['tabpositionbottomright'] = '右最下部';
$string['tabpositionbottomleft'] = '左最下部';
$string['tabpositionverticalleft'] = '左側';
$string['tabpositionverticalright'] = '右側';
$string['hideprivacytab'] = 'プライバシー設定タブを隠す';
$string['hideprivacytabdesc'] = 'オリジナルの埋め込みプライバシー設定リンクを使用する場合 (例 あなたのテーマテンプレート内)、あなたは標準プライバシー設定タブを無効にしても良いでしょう。';
$string['featureoptions'] = '機能オプション';
$string['pagerefresh'] = 'ページリフレッシュ';
$string['pagerefreshdesc'] = 'あなたにクッキー同意を認識する必要のあるサーバサイドアプリケーションがある場合、このオプションを選択することで同意後にページがリロードされます。';
$string['ignoredonottrack'] = '行動追跡拒否 (do not track) を無視する';
$string['ignoredonottrackdesc'] = 'この設定を有効にした場合、クッキーコンセントは訪問者のブラウザの「行動追跡拒否 (do not track) 」ヘッダを無視することを意味します。';
$string['usessl'] = 'SSLを使用する';
$string['usessldesc'] = 'SSLを使用してページにクッキーコンセントを表示する場合、あなたはこのオプションを選択する必要があります。';
$string['advertisingDefaultTitle'] = '広告';
$string['socialDefaultTitle'] = 'ソーシャルメディア';
$string['socialDefaultDescription'] = 'Facebook、Twitterおよび他のソーシャルウェブサイトが適切に動作するためには、あなたが誰であるか知る必要があります。';
$string['analyticsDefaultTitle'] = 'アナリティクス';
$string['analyticsDefaultDescription'] = 'あなたの経験を改善するため、このウェブサイトサイトの利用に関して、匿名で記録します。';
$string['advertisingDefaultDescription'] = '過去の行動および興味に基づき、あなたに広告が自動的に選択されます。';
$string['necessaryDefaultTitle'] = '厳密に必要';
$string['necessaryDefaultDescription'] = 'このウェブサイトのいくつかのクッキーは厳密に必要であるため、無効にすることができません。';
$string['defaultTitle'] = 'デフォルトクッキータイトル';
$string['defaultDescription'] = 'デフォルトクッキー説明';
$string['learnMore'] = '詳細情報';
$string['closeWindow'] = 'ウィンドウを閉じる';
$string['notificationTitle'] = 'このサイトにおけるあなたの経験は次のクッキーにより改善されます';
$string['notificationTitleImplicit'] = 'あなたが私たちのウェブサイトで最良の経験を得ることを保証するため、私たちはクッキーを使用します。';
$string['customCookie'] = 'このウェブサイトでは特定の承認が必要なカスタムタイプのクッキーを使用しています。';
$string['seeDetails'] = '詳細を表示する';
$string['seeDetailsImplicit'] = 'あなたの設定を変更する';
$string['hideDetails'] = '詳細を隠す';
$string['allowCookies'] = 'クッキーを許可する';
$string['allowCookiesImplicit'] = '閉じる';
$string['allowForAllSites'] = 'すべてのサイトに許可する';
$string['savePreference'] = 'プリファレンスを保存する';
$string['saveForAllSites'] = 'すべてのサイトに保存する';
$string['privacySettings'] = 'プライバシー設定';
$string['privacySettingsDialogTitleA'] = 'プライバシー設定';
$string['privacySettingsDialogTitleB'] = '(このウェブサイトに対して)';
$string['privacySettingsDialogSubtitle'] = 'あなたが誰であるか記憶するため、このサイトのいくつかの機能ではあなたの同意を必要とします。';
$string['changeForAllSitesLink'] = 'すべてのウェブサイトの設定を変更する';
$string['preferenceUseGlobal'] = 'グローバル設定を使用する';
$string['preferenceConsent'] = '同意します';
$string['preferenceDecline'] = '同意しません';
$string['notUsingCookies'] = 'このウェブサイトはクッキーを使用しません。';
$string['allSitesSettingsDialogTitleA'] = 'プライバシー設定';
$string['allSitesSettingsDialogTitleB'] = '(すべてのウェブサイトに対して)';
$string['allSitesSettingsDialogSubtitle'] = 'このプラグインを使用するため、あなたはすべてのウェブサイトに対して、これらのクッキーに同意することができます。';
$string['backToSiteSettings'] = 'ウェブサイト設定に戻る';
$string['preferenceAsk'] = '私に毎回尋ねる';
$string['preferenceAlways'] = '常に許可する';
$string['preferenceNever'] = '許可しない';

?>

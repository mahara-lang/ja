<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-11-30 04:06:41 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'プロファイル';
$string['profile'] = 'プロファイル';
$string['mandatoryfields'] = '必須フィールド';
$string['mandatoryfieldsdescription'] = '入力する必要のあるプロファイルフィールド';
$string['searchablefields'] = '検索対象フィールド';
$string['searchablefieldsdescription'] = 'ユーザにより検索することのできるプロファイルフィールド';
$string['aboutdescription'] = 'あなたの本名をここに入力してください。あなたがシステム内のユーザに異なる名前を表示したい場合、ニックネーム欄に別名を入力してください。';
$string['infoisprivate'] = 'あなたが他のユーザと共有するため、ページに置かない限り、この情報はプライベート (非公開) となります。';
$string['viewmyprofile'] = '私のプロファイルを表示する';
$string['aboutprofilelinkdescription'] = '<p>他のユーザに表示したい情報を編集するには、あなたの<a href="%s">プロファイル</a>ページにアクセスしてください。</p>';
$string['aboutme'] = '私について';
$string['contact'] = '連絡先';
$string['social'] = 'ソーシャルメディア';
$string['messaging'] = 'ソーシャルメディア';
$string['firstname'] = '名';
$string['lastname'] = '姓';
$string['fullname'] = 'フルネーム';
$string['institution'] = 'インスティテューション';
$string['studentid'] = '学籍番号';
$string['preferredname'] = 'ニックネーム';
$string['introduction'] = '自己紹介';
$string['email'] = 'メールアドレス';
$string['maildisabled'] = 'Eメール無効';
$string['officialwebsite'] = 'オフィシャルウェブサイトURL';
$string['personalwebsite'] = 'パーソナルウェブサイトURL';
$string['blogaddress'] = 'ブログアドレス';
$string['address'] = '番地以下';
$string['town'] = '市区町村';
$string['city'] = '都道府県';
$string['country'] = '国';
$string['homenumber'] = '自宅電話';
$string['businessnumber'] = '勤務先電話';
$string['mobilenumber'] = '携帯電話';
$string['faxnumber'] = 'FAX番号';
$string['aim.input'] = 'AIMスクリーン名';
$string['aim'] = 'AIM';
$string['icq.input'] = 'ICQナンバー';
$string['icq'] = 'ICQ';
$string['jabber.input'] = 'Jabberユーザ名';
$string['jabber'] = 'Jabber';
$string['skype.input'] = 'Skypeユーザ名';
$string['skype'] = 'Skype';
$string['yahoo.input'] = 'Yahoo!メッセンジャー';
$string['yahoo'] = 'Yahoo!メッセンジャー';
$string['facebook.input'] = 'Facebook URL';
$string['facebook'] = 'Facebook';
$string['twitter.input'] = 'Twitterユーザ名';
$string['twitter'] = 'Twitter';
$string['instagram.input'] = 'Instagramユーザ名';
$string['instagram'] = 'Instagram';
$string['tumblr.input'] = 'Tumblr URL';
$string['tumblr'] = 'Tumblr';
$string['pinterest.input'] = 'Pinterestユーザ名';
$string['pinterest'] = 'Pinterest';
$string['occupation'] = '職業';
$string['industry'] = '職種';
$string['name'] = '名称';
$string['principalemailaddress'] = '主メールアドレス';
$string['emailaddress'] = '代替メールアドレス';
$string['saveprofile'] = 'プロファイルを保存する';
$string['profilesaved'] = 'プロファイルが正常に保存されました。';
$string['profilefailedsaved'] = 'プロファイルの保存に失敗しました。';
$string['emailvalidation_subject'] = 'メールアドレスの確認';
$string['emailvalidation_body1'] = '%s さん、こんにちは。

Mahara内のあなたのユーザアカウントにメールアドレス「 %s 」が追加されました。下記リンクにアクセスして、このメールアドレスを有効化してください。

%s

このメールアドレスがあなたのメールアドレスであり、あなたがMaharaのアカウント作成をリクエストしていない場合、以下のリンクをクリックして、メール有効化を拒否してください。

%s';
$string['validationemailwillbesent'] = 'あなたのプロファイル保存後、確認メールが送信されます。';
$string['validationemailsent'] = '確認メールが送信されました。';
$string['emailactivation'] = 'メール有効化';
$string['emailactivationsucceeded'] = 'メールが正常に有効化されました。';
$string['emailalreadyactivated'] = 'メールはすでに有効化されています。';
$string['emailactivationfailed'] = 'メール有効化に失敗しました。';
$string['emailactivationdeclined'] = 'メール有効化が正常に拒否されました。';
$string['verificationlinkexpired'] = '確認リンクの有効期限が切れました。';
$string['invalidemailaddress'] = '無効なメールアドレスです。';
$string['unvalidatedemailalreadytaken'] = 'あなたが確認しようとしているメールアドレスはすでに登録されています。';
$string['addbutton'] = '追加';
$string['cancelbutton'] = 'キャンセル';
$string['emailingfailed'] = 'プロファイルが保存されましたが、次のメールアドレス宛にメール送信できませんでした: %s';
$string['loseyourchanges'] = '変更を取り消してもよろしいですか?';
$string['Title'] = 'タイトル';
$string['Created'] = '作成日時';
$string['Description'] = '説明';
$string['Download'] = 'ダウンロード';
$string['lastmodified'] = '最終更新日時';
$string['Owner'] = 'オーナー';
$string['Preview'] = 'プレビュー';
$string['Size'] = 'サイズ';
$string['Type'] = 'タイプ';
$string['profileinformation'] = 'プロファイル情報';
$string['profilepage'] = 'プロファイルページ';
$string['viewprofilepage'] = 'プロファイルページを表示する';
$string['viewallprofileinformation'] = 'すべてのプロファイル情報を表示する';
$string['Note'] = 'ノート';
$string['Notes'] = 'ノート';
$string['mynotes'] = 'マイノート';
$string['notesfor'] = '%s のノート';
$string['containedin'] = '含まれる場所:';
$string['currenttitle'] = 'タイトル';
$string['notesdescription1'] = 'あなたのページのノートブロック内で作成したHTML形式のノートです。';
$string['editnote'] = 'ノートを編集する';
$string['confirmdeletenote'] = 'このノートは %d ブロックおよび %d ページで使用されています。あなたが削除した場合、現在このテキストを含むすべてのブロックには空白が表示されてしまいます。';
$string['notedeleted'] = 'ノートが削除されました。';
$string['noteupdated'] = 'ノートが更新されました。';
$string['html'] = 'ノート';
$string['duplicatedprofilefieldvalue'] = '複製値';
$string['existingprofilefieldvalues'] = '既存値';
$string['progressbaritem_messaging'] = 'メッセージング';
$string['progressbaritem_joingroup'] = 'グループに参加する';
$string['progressbaritem_makefriend'] = 'フレンドを作る';
$string['progress_firstname'] = 'あなたの名を追加する';
$string['progress_lastname'] = 'あなたの姓を追加する';
$string['progress_studentid'] = 'あなたの学籍番号を追加する';
$string['progress_preferredname'] = 'あなたの表示名を追加する';
$string['progress_introduction'] = 'あなたの自己紹介を追加する';
$string['progress_email'] = 'あなたのメールアドレスを追加する';
$string['progress_officialwebsite'] = 'あなたのオフィシャルウェブサイトを追加する';
$string['progress_personalwebsite'] = 'あなたのパーソナルウェブサイトを追加する';
$string['progress_blogaddress'] = 'あなたのブログアドレスを追加する';
$string['progress_address'] = 'あなたの番地以下を追加する';
$string['progress_town'] = 'あなたの市町村を追加する';
$string['progress_city'] = 'あなたの都道府県を追加する';
$string['progress_country'] = 'あなたの国を追加する';
$string['progress_homenumber'] = 'あなたの自宅電話を追加する';
$string['progress_businessnumber'] = 'あなたの勤務先電話を追加する';
$string['progress_mobilenumber'] = 'あなたの携帯電話を追加する';
$string['progress_faxnumber'] = 'あなたのFAX番号を追加する';
$string['progress_messaging'] = 'あなたのメッセージング情報を追加する';
$string['progress_occupation'] = 'あなたの職業を追加する';
$string['progress_industry'] = 'あなたの職種を追加する';
$string['progress_joingroup'] = '%s グループに参加する';
$string['progress_makefriend'] = '%s フレンドを作る';
$string['socialprofile'] = 'ソーシャルプロファイル';
$string['socialprofiles'] = 'ソーシャルプロファイル';
$string['service'] = 'ソーシャルサイト名';
$string['servicedesc'] = 'ソーシャルサイトの名称を入力してください。例) Facebook、LinkedIn、Twitter等';
$string['profileurl'] = 'ソーシャルプロファイルURL';
$string['profileurldesc'] = '古いメッセージングサービスに関して、あなたのユーザ名、スクリーン名またはナンバーを入力してください。そうでない場合、あなたのソーシャルプロファイルのURLを入力してください。';
$string['profileurlexists'] = 'すでに存在しているため、このソーシャルプロファイルURLを追加することはできません。';
$string['profiletype'] = 'ソーシャルプロファイルタイプ';
$string['deleteprofile'] = 'ソーシャルプロファイルを削除する';
$string['deletethisprofile'] = 'ソーシャルプロファイルを削除する:「 %s 」';
$string['deleteprofileconfirm'] = '本当にこのソーシャルプロファイルを削除してもよろしいですか?';
$string['editthisprofile'] = 'ソーシャルプロファイルを編集する:「 %s 」';
$string['newsocialprofile'] = '新しいソーシャルプロファイル';
$string['notvalidprofileurl'] = 'これは有効なソーシャルプロファイルURLではありません。有効なURLを入力するか、上記リストより適切なメッセージングサービスを選択してください。';
$string['profiledeletedsuccessfully'] = 'ソーシャルプロファイルが正常に削除されました。';
$string['profilesavedsuccessfully'] = 'ソーシャルプロファイルが正常に保存されました。';
$string['socialprofilerequired'] = '少なくとも1つのソーシャルプロファイルが必要です。';
$string['duplicateurl'] = 'このソーシャルプロファイルURLは重複しています。';

?>

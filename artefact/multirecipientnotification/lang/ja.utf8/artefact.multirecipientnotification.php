<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2015-03-12 18:56:10 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['sendmessageto'] = 'メッセージを送信する';
$string['composemessage'] = '作成';
$string['composemessagedesc'] = '新しいメッセージを作成する';
$string['titlesubject'] = '件名';
$string['titlerecipient'] = '受信者';
$string['replysubjectprefix'] = 'Re:';
$string['labelrecipients'] = 'To:';
$string['labelsubject'] = '件名';
$string['deletednotifications1'] = '%s 件の通知を削除しました。「送信済み」エリアからは内部通知を削除できないことに留意してください!';
$string['notification'] = '通知';
$string['cantsendemptysubject'] = 'あなたの件名は空白です。件名を入力してください。';
$string['cantsendemptytext'] = 'あなたのメッセージは空白です。メッセージを入力してください。';
$string['cantsendnorecipients'] = '少なくとも1名の受信者を選択してください。';
$string['removeduserfromlist'] = 'あなたのメッセージを受信できないユーザは受信リストから削除されています。';
$string['deleteduser'] = '削除済みユーザ';
$string['fromuser'] = 'From';
$string['touser'] = 'To';
$string['reply'] = '返信';
$string['replyall'] = 'すべてに返信する';
$string['linkindicator'] = '»';
$string['labeloutbox1'] = '送信済み';
$string['outboxdesc'] = '他のユーザに送信されたメッセージ';
$string['labelinbox'] = '受信箱';
$string['inboxdesc'] = 'Maharaシステムおよび他のユーザから受信したメッセージ';
$string['selectallread'] = 'すべての未読通知';
$string['selectalldelete'] = '削除する通知すべて';
$string['clickformore'] = '(詳細情報を表示するには、「Enter」を押してください)';

?>

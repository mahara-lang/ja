<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2011-05-26 17:32:39 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['exactusersearch'] = 'ユーザ厳密検索';
$string['exactusersearchdescription'] = 'チェックした場合、「ユーザを検索する」ボックスおよび「フレンドを探す」ページの結果には、検索キーワード全体に合致するプロファイルフィールドのユーザのみ表示されます。';

?>

<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2015-03-01 19:45:48 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'ノート';
$string['description'] = 'あなたのページにテキストボックスを追加します。';
$string['blockcontent'] = 'ブロックコンテンツ';
$string['usecontentfromanothertextbox1'] = '別のノートのコンテンツを使用する';
$string['textusedinotherblocks'] = 'あなたがこのブロックのテキストを編集した場合、テキストが表示されている %s 件の他のブロックも更新されます。';
$string['managealltextboxcontent1'] = 'すべてのノートコンテンツを管理する';
$string['readonlymessage'] = 'あなたが選択したテキストはこのページで編集することはできません。';
$string['makeacopy'] = 'コピーを作成する';
$string['textusedinothernotes'] = 'あなたがこのノートのテキストを編集した場合、テキストが表示される %s ブロックでも変更されます。';

?>

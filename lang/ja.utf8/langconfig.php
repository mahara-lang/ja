<?php
/**
 *
 * @package    mahara
 * @subpackage lang (Japanese)
 * @translator Mitsuhiro Yoshida (http://mitstek.com/)
 * @started    2008-01-19 11:25:00 UTC
 * @updated    2014-10-13 22:21:51 UTC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['calendar_dateFormat'] = 'yy/mm/dd';
$string['calendar_timeFormat'] = 'HH:mm';
$string['strftimedate'] = '%%Y年 %%m月 %%d日';
$string['strftimedateshort'] = '%%m/%%d';
$string['strftimedatetime'] = '%%Y年 %%m月 %%d日 %%H:%%M';
$string['strftimedatetimeshort'] = '%%Y/%%m/%%d %%H:%%M';
$string['strftimedaydate'] = '%%Y年 %%m月 %%d日(%A)';
$string['strftimedaydatetime'] = '%%Y年 %%m月 %%d日(%%A) %%H:%%M';
$string['strftimedayshort'] = '%%Y年 %%m月 %%d日';
$string['strftimedaytime'] = '(%%a) %%H:%%M';
$string['strftimemonthyear'] = '%%Y年 %%m月';
$string['strftimenotspecified'] = '未指定';
$string['strftimerecent'] = '%%m月 %%d日 %%H:%%M';
$string['strftimerecentyear'] = '%%Y年 %%m月 %%d日 %%H:%%M';
$string['strftimerecentfull'] = '%%Y年 %%m月 %%d日(%%a) %%H:%%M';
$string['strftimetime'] = '%%H:%%M';
$string['strfdaymonthyearshort'] = '%%Y/%%m/%%d';
$string['strfdateofbirth'] = '%%Y/%%m/%%d';
$string['strftimew3cdatetime'] = '%%Y-%%m-%%dT%%H:%%M:%%S%%z';
$string['strftimew3cdate'] = '%%Y-%%m-%%d';
$string['thislanguage'] = '日本語';
$string['locales'] = 'ja_JP.utf8,Japanese_Japan.932';
$string['pluralrule'] = '0';
$string['pluralfunction'] = 'plural_ja_utf8';

?>
